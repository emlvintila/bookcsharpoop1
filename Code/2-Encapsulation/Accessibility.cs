﻿namespace Encapsulation.Accessiblity
{
    public class Program
    {
        private /*static*/ void Main(string[] args)
        {
            PublicClass instance = new PublicClass();
            instance.internalField = "Internal fields are accessible.";
            instance.publicField = 3.14159;
            // private members are not accessible
            // instance.privateField = instance.privateConst * 5;
            // readonly members cannot be assigned
            // instance.readonlyField = double.PositiveInfinity;
            // instance.ReadonlyProperty = double.MaxValue;
        }
    }

    public class PublicClass
    {
        private const int privateConst = 42;
        private int privateField;
        internal string internalField;
        public double publicField;

        public readonly double readonlyField;
        public double ReadonlyProperty { get; }

        public PublicClass()
        {
            readonlyField = 2.71828;
            ReadonlyProperty = 2.71828;
        }
    }
}