using System.IO;

namespace Encapsulation.Reference
{
    public class ReferenceProblem
    {
        private static void Main(string[] args)
        {
            Logger logger = new Logger("log.txt");
            Worker worker = new Worker(logger);
            worker.DoWork();
            // the following statement is NOT valid, because the logger field is inaccessible
            // service.logger.CloseFile();
            // the following statement closes the Logger associated with the Worker instance above
            logger.CloseFile();
            // now the following call throws an Exception
            worker.DoWork();
        }
    }

    public class Worker
    {
        private Logger logger;

        public Worker(Logger logger) => this.logger = logger;

        public void DoWork()
        {
            // do something here...
            logger.Log("Did some work.");
        }
    }

    public class Logger
    {
        private StreamWriter file;

        public Logger(string filename) => file = new StreamWriter(filename);
        
        public void Log(string message) => file.WriteLine(message);

        public void CloseFile() => file.Close();
    }
}