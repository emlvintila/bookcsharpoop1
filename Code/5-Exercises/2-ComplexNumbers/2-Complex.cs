using System;

namespace Exercises.ComplexNumbers
{
    public struct Complex : IEquatable<Complex>
    {
        public float Real { get; }
        public float Imaginary { get; }

        public Complex(float real, float imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }

        public Complex Conjugate() => new Complex(Real, -Imaginary);

        public float AbsoluteValue() => (float) Math.Sqrt(Real * Real + Imaginary * Imaginary);

        public static implicit operator Complex(float real) => new Complex(real, 0);

        public static Complex operator +(Complex left, Complex right) =>
            new Complex(left.Real + right.Real, left.Imaginary + right.Imaginary);

        public static Complex operator -(Complex left, Complex right) =>
            new Complex(left.Real - right.Real, left.Imaginary - right.Imaginary);

        public static Complex operator -(Complex complex) => new Complex(-complex.Real, -complex.Imaginary);

        public static Complex operator *(Complex left, Complex right) =>
            new Complex(left.Real * right.Real - left.Imaginary * right.Imaginary,
                left.Real * right.Imaginary + left.Imaginary * right.Real);

        public static Complex operator /(Complex left, Complex right)
        {
            Complex numerator = left * right.Conjugate();
            float abs = right.AbsoluteValue();
            float abssqr = abs * abs;
            return new Complex(numerator.Real / abssqr, numerator.Imaginary / abssqr);
        }

        public bool Equals(Complex other)
        {
            const float DELTA = 1e-3f;
            return Math.Abs(Real - other.Real) < DELTA && Math.Abs(Imaginary - other.Imaginary) < DELTA;
        }

        public override bool Equals(object obj) => obj is Complex other && Equals(other);

        public static bool operator ==(Complex left, Complex right) => left.Equals(right);

        public static bool operator !=(Complex left, Complex right) => !left.Equals(right);

        public override string ToString() => $"{Real:F2} + {Imaginary:F2} i";
    }
}