using NUnit.Framework;

namespace Exercises.ComplexNumbers
{
    [TestFixture]
    [DefaultFloatingPointTolerance(1e-3f)]
    public class ComplexTests
    {
        [Test]
        public void TestAddition()
        {
            Complex c1 = new Complex(4, 2);
            Complex c2 = new Complex(8, 6);

            Assert.AreEqual(new Complex(12, 8), c1 + c2);
            Assert.AreEqual(new Complex(5, 2), 1 + c1);
        }

        [Test]
        public void TestSubtraction()
        {
            Complex c1 = new Complex(3 , 7);
            Complex c2 = new Complex(15, 3);
            Complex expected = new Complex(-12, 4);
            
            Assert.AreEqual(expected, c1 - c2);
            Assert.AreEqual(expected, -(c2 - c1));
            Assert.AreEqual(c1, -(-c1));
        }

        [Test]
        public void TestMultiplication()
        {
            Complex c1 = new Complex(2, 3);
            Complex c2 = new Complex(4, 5);
            Complex expected = new Complex(-7, 22);

            Assert.AreEqual(expected, c1 * c2);
            Assert.AreEqual(new Complex(4, 6), 2 * c1);
        }
        
        [Test]
        public void TestDivision()
        {
            Complex c1 = new Complex(3, 5);
            Complex c2 = new Complex(6, 4);
            Complex expected = new Complex(38, 18) / 52;
            
            Assert.AreEqual(expected, c1 / c2);
            Assert.AreEqual(1 / expected, c2 / c1);
        }

        [Test]
        public void TestFloatConversion()
        {
            Complex complex = 42;
            Complex expected = new Complex(42, 0);
            
            Assert.AreEqual(expected, complex);
        }

        [Test]
        public void TestConjugate()
        {
            Complex conjugate = new Complex(5, 4).Conjugate();
            Complex expected = new Complex(5, -4);
            
            Assert.AreEqual(expected, conjugate);
        }

        [Test]
        public void TestAbsoluteValue()
        {
            Complex complex = new Complex(3, 4);
            Complex negative = new Complex(-3, -4);
            Complex altern = new Complex(-3, 4);
            
            Assert.AreEqual(5, complex.AbsoluteValue());
            Assert.AreEqual(5, negative.AbsoluteValue());
            Assert.AreEqual(5, altern.AbsoluteValue());
        }
    }
}