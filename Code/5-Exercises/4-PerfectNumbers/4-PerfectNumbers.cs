using System;
using System.Collections.Generic;
using System.Linq;
using Exercises.Divisors;

namespace Exercies.PerfectNumbers
{
    public static class PerfectNumbers
    {
        public static NumberType Type(int number)
        {
            if (number <= 0)
                throw new ArgumentException();
            IEnumerable<int> divisors = Divisors.Of(number).Where(divisor => divisor != number);
            int sum = divisors.Sum();
            if (sum > number)
                return NumberType.Abundant;
            if (sum == number)
                return NumberType.Perfect;
            return NumberType.Deficient;
        }
    }
    
    public enum NumberType
    {
        Deficient,
        Perfect,
        Abundant
    }
}
