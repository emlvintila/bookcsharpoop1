using System;
using NUnit.Framework;

namespace Exercies.PerfectNumbers
{
    [TestFixture]
    public class PerfectNumbersTests
    {
        [Test]
        public void TestPerfect()
        {
            Assert.AreEqual(NumberType.Perfect, PerfectNumbers.Type(6));
            Assert.AreEqual(NumberType.Perfect, PerfectNumbers.Type(28));
        }

        [Test]
        public void TestDeficient()
        {
            Assert.AreEqual(NumberType.Deficient, PerfectNumbers.Type(7));
            Assert.AreEqual(NumberType.Deficient, PerfectNumbers.Type(13));
            Assert.AreEqual(NumberType.Deficient, PerfectNumbers.Type(15));
            Assert.AreEqual(NumberType.Deficient, PerfectNumbers.Type(17));
        }

        [Test]
        public void TestAbundant()
        {
            Assert.AreEqual(NumberType.Abundant, PerfectNumbers.Type(12));
            Assert.AreEqual(NumberType.Abundant, PerfectNumbers.Type(24));
        }
        
        [Test]
        public void TestOne()
        {
            Assert.AreEqual(NumberType.Deficient, PerfectNumbers.Type(1));       
        }

        [Test]
        public void TestNonPositive()
        {
            Assert.Throws<ArgumentException>(() => PerfectNumbers.Type(0));
            Assert.Throws<ArgumentException>(() => PerfectNumbers.Type(-1));
        }
    }
}
