﻿using System;

namespace Exercises.Shapes
{
    public interface IShape
    {
        float GetArea();

        float GetPerimeter();
    }

    public class Rectangle : IShape
    {
        private readonly float width;
        private readonly float height;

        public float Diagonal { get; }

        public Rectangle(float width, float height)
        {
            if (width <= 0 || height <= 0)
                throw new ArgumentOutOfRangeException();
            if (float.IsNaN(width) || float.IsNaN(height))
                throw new ArgumentException();

            this.width = width;
            this.height = height;
            Diagonal = (float) Math.Sqrt(width * width + height * height);
        }

        public float GetArea() => width * height;

        public float GetPerimeter() => 2 * (width + height);
    }

    public class Square : Rectangle
    {
        public Square(float length) : base(length, length) { }
    }

    public class Circle : IShape
    {
        private readonly float radius;

        public Circle(float radius)
        {
            if (radius <= 0)
                throw new ArgumentOutOfRangeException();
            if (float.IsNaN(radius))
                throw new ArgumentException();

            this.radius = radius;
        }

        public float GetArea() => (float) (radius * Math.PI * Math.PI);

        public float GetPerimeter() => (float) (2 * radius * Math.PI);
    }
}
