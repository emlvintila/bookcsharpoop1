﻿using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace Exercises.Shapes
{
    [TestFixture]
    [DefaultFloatingPointTolerance(DELTA_D)]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
    public class ShapeTests
    {
        private const double DELTA_D = 1e-3;

        [Test]
        public void TestArea()
        {
            IShape rectangle = new Rectangle(3, 4);
            Assert.AreEqual(12, rectangle.GetArea());

            IShape square = new Square(5);
            Assert.AreEqual(25, square.GetArea());

            IShape circle = new Circle(6);
            Assert.AreEqual(6 * Math.PI * Math.PI, circle.GetArea());
        }

        [Test]
        public void TestPerimeter()
        {
            IShape rectangle = new Rectangle(3, 4);
            Assert.AreEqual(14, rectangle.GetPerimeter());

            IShape square = new Square(5);
            Assert.AreEqual(20, square.GetPerimeter());

            IShape circle = new Circle(6);
            Assert.AreEqual(12 * Math.PI, circle.GetPerimeter());
        }

        [Test]
        public void TestDiagonal()
        {
            Rectangle rectangle = new Rectangle(3, 4);
            Assert.AreEqual(5, rectangle.Diagonal);

            Rectangle square = new Square(5);
            Assert.AreEqual(5 * Math.Sqrt(2), square.Diagonal);
        }

        [Test]
        public void TestNegativeLengthThrows([Random(float.MinValue, -1, 5)] float negative)
        {
            float positive = -negative;

            Assert.Throws<ArgumentOutOfRangeException>(() => new Rectangle(negative, negative));
            Assert.Throws<ArgumentOutOfRangeException>(() => new Rectangle(negative, positive));
            Assert.Throws<ArgumentOutOfRangeException>(() => new Rectangle(positive, negative));

            Assert.Throws<ArgumentOutOfRangeException>(() => new Square(negative));

            Assert.Throws<ArgumentOutOfRangeException>(() => new Circle(negative));
        }

        [Test]
        public void TestNaNThrows()
        {
            Assert.Throws<ArgumentException>(() => new Rectangle(float.NaN, 1));
            Assert.Throws<ArgumentException>(() => new Rectangle(1, float.NaN));
            Assert.Throws<ArgumentException>(() => new Rectangle(float.NaN, float.NaN));

            Assert.Throws<ArgumentException>(() => new Square(float.NaN));

            Assert.Throws<ArgumentException>(() => new Circle(float.NaN));
        }
    }
}
