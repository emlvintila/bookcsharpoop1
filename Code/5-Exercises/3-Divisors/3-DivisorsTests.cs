using System.Collections.Generic;
using System.Collections.Immutable;
using NUnit.Framework;

namespace Exercises.Divisors
{
    [TestFixture]
    public class DivisorsTests
    {
        [Test]
        public void TestDivisors()
        {
            IEnumerable<int> expected = new[] {1, 2, 3, 6};
            IEnumerable<int> actual = Divisors.Of(6).ToImmutableSortedSet();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestNegative()
        {
            IEnumerable<int> expected = new[] {1, 2, 4};
            IEnumerable<int> negative = Divisors.Of(-4).ToImmutableSortedSet();
            
            Assert.AreEqual(expected, negative);
        }

        [Test]
        public void TestPrime()
        {
            IEnumerable<int> expected = new[] {1, 7};
            IEnumerable<int> primes = Divisors.Of(7).ToImmutableSortedSet();
            
            Assert.AreEqual(expected, primes);
        }

        [Test]
        public void TestOne()
        {
            IEnumerable<int> expected = new[] {1};
            IEnumerable<int> actual = Divisors.Of(1).ToImmutableSortedSet();
            
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestZero()
        {
            IEnumerable<int> actual = Divisors.Of(0);
            
            Assert.IsEmpty(actual);
        }
    }
}