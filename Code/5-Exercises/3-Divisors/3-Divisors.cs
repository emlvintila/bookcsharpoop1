using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercises.Divisors
{
    public static class Divisors
    {
        public static IEnumerable<int> Of(int number)
        {
            if (number == 0)
                return new int[0];

            number = Math.Abs(number);
            List<int> divisors = new List<int>();
            for (int i = 1, high = number / 2; i <= high; ++i)
                if (number % i == 0)
                    divisors.Add(i);
            divisors.Add(number);

            return divisors;
        }
    }
}