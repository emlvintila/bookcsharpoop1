using NUnit.Framework;

namespace Exercises.EditDistance
{
    [TestFixture]
    public class LevenshteinDistanceTests
    {
        [Test]
        public void Test()
        {
            Assert.AreEqual(0, EditDistance.LevenshteinDistance("levenshtein", "levenshtein"));
            Assert.AreEqual(3, EditDistance.LevenshteinDistance("kitten", "sitting"));
            Assert.AreEqual(3, EditDistance.LevenshteinDistance("sitting", "kitten"));
        }
        
        [Test]
        public void TestCaseInsensitive()
        {
            const string lower = "lowercase";
            const string mixed = "LoWeRcAsE";
            
            Assert.AreEqual(0, EditDistance.LevenshteinDistance(lower, mixed));
        }

        [Test]
        public void TestSymbols()
        {
            const string alpha = "abcde";
            const string symbols = "!@#$%";
            const string mixed = "ab#de";
            
            Assert.AreEqual(5, EditDistance.LevenshteinDistance(alpha, symbols));
            Assert.AreEqual(1, EditDistance.LevenshteinDistance(alpha, mixed));
            Assert.AreEqual(4, EditDistance.LevenshteinDistance(symbols, mixed));
        }
        
        [Test]
        public void TestEmptyString()
        {
            const string str = "NotEmpty";
            
            Assert.AreEqual(str.Length, EditDistance.LevenshteinDistance(str, ""));
            Assert.AreEqual(str.Length, EditDistance.LevenshteinDistance("", str));
        }

        [Test]
        public void TestNullString()
        {
            const string str = "NotNull";
            
            Assert.AreEqual(str.Length, EditDistance.LevenshteinDistance(str, null));
            Assert.AreEqual(str.Length, EditDistance.LevenshteinDistance(null, str));
        }

        [Test]
        public void TestBothNullOrEmpty()
        {
            Assert.AreEqual(0, EditDistance.LevenshteinDistance(null, null));
            Assert.AreEqual(0, EditDistance.LevenshteinDistance(null, ""));
            Assert.AreEqual(0, EditDistance.LevenshteinDistance("", null));
            Assert.AreEqual(0, EditDistance.LevenshteinDistance("", ""));
        }
    }
}