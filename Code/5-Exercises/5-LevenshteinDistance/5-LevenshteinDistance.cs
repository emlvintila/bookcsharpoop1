using System;
using System.Linq;

namespace Exercises.EditDistance
{
    public static class EditDistance
    {
        public static int LevenshteinDistance(string left, string right)
        {
            left = left is null ? string.Empty : left.ToLowerInvariant();
            right = right is null ? string.Empty : right.ToLowerInvariant();

            int[] vleft = Enumerable.Range(0, right.Length + 1).ToArray();
            int[] vright = new int[right.Length + 1];

            void Swap(ref int[] leftArray, ref int[] rightArray)
            {
                int[] temp = leftArray;
                leftArray = rightArray;
                rightArray = temp;
            }

            for (int i = 0; i < left.Length; ++i)
            {
                vright[0] = i + 1;
                for (int j = 0; j < right.Length; ++j)
                {
                    int deletionCost = vleft[j + 1] + 1;
                    int insertionCost = vright[j] + 1;
                    int substitutionCost = vleft[j];
                    if (left[i] != right[j])
                        ++substitutionCost;

                    vright[j + 1] = new[] {deletionCost, insertionCost, substitutionCost}.Min();
                }

                Swap(ref vleft, ref vright);
            }

            return vleft.Last();
        }
    }
}