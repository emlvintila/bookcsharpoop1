using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Exercises.Stack
{
    [TestFixture]
    public class StackTests
    {
        [Test]
        public void TestCount()
        {
            IEnumerable<int> ints = new[] {1, 2, 3};
            Stack<int> stack = new Stack<int>();
            foreach (int i in ints)
                stack.Push(i);
            
            Assert.AreEqual(ints.Count(), stack.Count);
        }

        [Test]
        public void TestEmpty()
        {
            Stack<int> stack = new Stack<int>();
            Assert.IsTrue(stack.IsEmpty());
            stack.Push(42);
            Assert.IsFalse(stack.IsEmpty());
        }

        [Test]
        public void TestEmptyPopThrows()
        {
            Stack<int> stack = new Stack<int>();
            Assert.Throws<InvalidOperationException>(() => stack.Pop());
        }

        [Test]
        public void TestLifo()
        {
            IEnumerable<int> ints = new[] {1, 2, 3, 4, 5};
            Stack<int> stack = new Stack<int>();
            foreach (int i in ints)
                stack.Push(i);
            List<int> popped = new List<int>(ints.Count());
            while (!stack.IsEmpty())
                popped.Add(stack.Pop());
            
            Assert.AreEqual(popped, ints.Reverse());
        }

        [Test]
        public void TestBigSize()
        {
            const int size = 1000;
            Stack<int> stack = new Stack<int>();
            foreach (int i in Enumerable.Range(0, size))
                stack.Push(i);
            
            Assert.AreEqual(stack.Count, size);
        }
    }
}