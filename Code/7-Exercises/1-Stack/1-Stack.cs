using System;

namespace Exercises.Stack
{
    public class Stack<T>
    {
        private const int CAPACITY_INITIAL = 20;
        private const int CAPACITY_DELTA = 20;
        private int current;
        private T[] elements;

        public Stack()
        {
            current = -1;
            elements = new T[CAPACITY_INITIAL];
        }

        public int Count => current + 1;

        public void Push(T element)
        {
            if (current == elements.Length - 1)
            {
                T[] newElements = new T[elements.Length + CAPACITY_DELTA];
                Array.Copy(elements, newElements, elements.Length);
                elements = newElements;
            }

            elements[++current] = element;
        }

        public T Pop()
        {
            if (IsEmpty())
                throw new InvalidOperationException();
            T element = elements[current];
            elements[current] = default;
            --current;
            
            return element;
        }

        public bool IsEmpty() => current < 0;
    }
}