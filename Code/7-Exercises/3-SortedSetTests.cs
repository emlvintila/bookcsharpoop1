using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Exercises.SortedSet
{
    [TestFixture]
    public class SortedSetTests
    {
        [Test]
        public void TestAdd()
        {
            IEnumerable<int> ints = new[] {4, 1, 3, 7};
            IEnumerable<int> expected = new[] {1, 3, 4, 7};
            SortedSet<int> set = new SortedSet<int>();
            foreach (int i in ints)
                set.Add(i);

            Assert.AreEqual(expected, set);
        }

        [Test]
        public void TestAddMany()
        {
            IEnumerable<int> ints = Enumerable.Range(0, 1000);
            SortedSet<int> set = new SortedSet<int>();
            foreach (int i in ints)
                set.Add(i);
            Assert.AreEqual(ints, set);
        }

        [Test]
        public void TestCount()
        {
            SortedSet<int> set = new SortedSet<int>();
            for (int i = 1; i <= 5; ++i)
            for (int j = 1; j <= 5; ++j)
            {
                set.Add(i);
                Assert.AreEqual(i, set.Count);
            }
        }

        [Test]
        public void TestIndexOf()
        {
            IEnumerable<int> ints = new[] {7, 6, 5, 4};
            SortedSet<int> set = new SortedSet<int>();
            foreach (int i in ints)
                set.Add(i);
            Assert.AreEqual(0, set.IndexOf(4));
            Assert.AreEqual(1, set.IndexOf(5));
            Assert.Less(set.IndexOf(42), 0);
        }

        [Test]
        public void TestRemove()
        {
            IEnumerable<int> ints = new[] {3, 2, 5, 8};
            SortedSet<int> set = new SortedSet<int>();
            foreach (int i in ints)
                set.Add(i);
            set.Remove(3);
            Assert.AreEqual(new[] {2, 5, 8}, set);
            for (int i = 1; i <= 2; ++i)
            {
                set.Remove(2);
                Assert.AreEqual(new[] {5, 8}, set);
            }

            set.Remove(8);
            Assert.AreEqual(new[] {5}, set);
            set.Remove(5);
            Assert.IsEmpty(set);
        }
    }
}