using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Exercises.LinkedList
{
    [TestFixture]
    public class LinkedListTests
    {
        [Test]
        public void Test()
        {
            IEnumerable<int> ints = new[] {1, 2, 3, 4, 5};
            LinkedList<int> list = new LinkedList<int>();
            Assert.IsEmpty(list);
            foreach (int i in ints)
                list.Add(i);
            Assert.AreEqual(ints, list);
            list.Remove(ints.First());
            Assert.AreEqual(ints.Skip(1), list);
            list.Remove(ints.Skip(1).First());
            Assert.AreEqual(ints.Skip(2), list);
            foreach (int i in ints)
                list.Remove(i);
            Assert.IsEmpty(list);
        }
    }
}