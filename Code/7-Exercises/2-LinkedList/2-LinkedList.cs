using System.Collections;
using System.Collections.Generic;

namespace Exercises.LinkedList
{
    public class LinkedList<T> : IEnumerable<T>
    {
        private Node head;
        
        private class Node
        {
            public T Value { get; set; }
            public Node Next { get; set; }
            
            public Node(T value, Node next)
            {
                Value = value;
                Next = next;
            }
        }

        public void Add(T value)
        {
            if (head is null)
                head = new Node(value, null);
            else
            {
                Node current = head;
                while (!(current.Next is null))
                    current = current.Next;
                current.Next = new Node(value, null);
            }
        }

        public void Remove(T value)
        {
            for (Node current = head, prev = null; !(current is null); prev = current, current = current.Next)
                if (current.Value.Equals(value))
                {
                    if (prev != null)
                        prev.Next = current.Next;
                    else if (!(head is null))
                        head = head.Next;
                    return;
                }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (Node current = head; !(current is null); current = current.Next)
                yield return current.Value;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}