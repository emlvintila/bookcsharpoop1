using System;
using System.Collections;
using System.Collections.Generic;

namespace Exercises.SortedSet
{
    public class SortedSet<T> : IEnumerable<T>
        where T : IComparable<T>
    {
        private const int CAPACITY_INITIAL = 20;
        private const int CAPACITY_DELTA = 20;
        private int current;
        private T[] elements;

        public SortedSet()
        {
            elements = new T[CAPACITY_INITIAL];
            current = -1;
        }

        public int Count => current + 1;

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i <= current; ++i)
                yield return elements[i];
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public void Add(T element)
        {
            if (current == elements.Length - 1)
            {
                T[] newElements = new T[elements.Length + CAPACITY_DELTA];
                Array.Copy(elements, newElements, elements.Length);
                elements = newElements;
            }

            switch (Count)
            {
                case 0:
                    elements[++current] = element;
                    return;
                case 1:
                    int compare = element.CompareTo(elements[0]);
                    if (compare < 0)
                    {
                        ShiftRight(0);
                        elements[current++] = element;
                    }
                    else if (compare > 0)
                    {
                        elements[++current] = element;
                    }

                    return;
                default:
                    if (element.CompareTo(elements[current]) > 0)
                    {
                        elements[++current] = element;
                        return;
                    }

                    if (element.CompareTo(elements[0]) < 0)
                    {
                        ShiftRight(0);
                        elements[0] = element;
                        ++current;
                        return;
                    }

                    for (int low = 0, high = current; low <= high;)
                    {
                        int middle = (low + high) / 2;
                        int next = middle + 1;

                        if (elements[middle].CompareTo(element) < 0)
                        {
                            if (elements[next].CompareTo(element) > 0)
                            {
                                ShiftRight(next);
                                elements[next] = element;
                                ++current;
                                return;
                            }

                            low = middle + 1;
                        }
                        else
                        {
                            high = middle - 1;
                        }
                    }

                    return;
            }
        }

        public void Remove(T element)
        {
            int index = IndexOf(element);
            if (index == -1)
                return;

            ShiftLeft(index);
            elements[current] = default;
            --current;
        }

        public int IndexOf(T element)
        {
            for (int low = 0, high = current; low <= high;)
            {
                int middle = (low + high) / 2;
                int compare = element.CompareTo(elements[middle]);
                if (compare == 0)
                    return middle;
                if (compare < 0)
                    high = middle - 1;
                else
                    low = middle + 1;
            }

            return -1;
        }

        private void ShiftLeft(int from)
        {
            for (int i = from; i < current; ++i)
                elements[i] = elements[i + 1];
        }

        private void ShiftRight(int from)
        {
            for (int i = current; i >= from; --i)
                elements[i + 1] = elements[i];
        }
    }
}