using System;
using System.Collections.Generic;

namespace Events.ObserverPattern
{
    public class Program
    {
        private static void Main(string[] args)
        {
            List<int> list = new List<int>();
            Observer<int> observer = new Observer<int>();
            // the following are equivalent
            // IDisposable unsubscriber =  observer.Register(list);
            IDisposable unsubscriber = list.Subscribe(observer);
            
            list.Add(42);
            unsubscriber.Dispose();
            list.Add(24);
        }
    }
    
    public class List<T> : IObservable<T>
    {
        public List() => Observers = new System.Collections.Generic.List<IObserver<T>>();

        private IList<IObserver<T>> Observers { get; }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (!Observers.Contains(observer))
                Observers.Add(observer);
            return new Unsubscriber<T>(Observers, observer);
        }

        public void Add(T item)
        {
            foreach (IObserver<T> observer in Observers)
                observer.OnNext(item);
        }
    }

    public class Unsubscriber<T> : IDisposable
    {
        private readonly IObserver<T> observer;
        private readonly IList<IObserver<T>> observers;

        public Unsubscriber(IList<IObserver<T>> observers, IObserver<T> observer)
        {
            this.observers = observers;
            this.observer = observer;
        }

        public void Dispose()
        {
            if (!(observer is null) && observers.Contains(observer))
                observers.Remove(observer);
        }

        ~Unsubscriber() => Dispose();
    }

    public class Observer<T> : IObserver<T>
    {
        public void OnCompleted() { }

        public void OnError(Exception error) { }

        public void OnNext(T value) => Console.WriteLine(value);

        public IDisposable Register(List<T> list) => list.Subscribe(this);
    }
}