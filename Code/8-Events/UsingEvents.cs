﻿using System;

namespace Events.UsingEvents
{
    public class Program
    {
        private /*static*/ void Main(string[] args)
        {
            List list = new List();
            list.ItemAdded += ListOnItemAdded;
            list.Add(42);
            list.ItemAdded -= ListOnItemAdded;
            list.Add(24);
            // the following calls are invalid, because they are not inside the List class
            // list.ItemAdded(15);
            // list.ItemAdded.Invoke(51);
        }

        private static void ListOnItemAdded(List list, object newitem)
        {
            Console.WriteLine("{0} was added to the list.", newitem);
        }
    }

    public class List
    {
        public delegate void ItemAddedDelegate(List list, object newItem);
        
        public event ItemAddedDelegate ItemAdded;

        public void Add(object obj)
        {
            if (!(ItemAdded is null))
                ItemAdded.Invoke(this, obj);
        }
    }
}