using System;

namespace Inheritance.Virtual
{
    public class Porgram
    {
        private static void Main(string[] args)
        {
            Base b = new Base();
            Base d = new Derived();
            Base h = new DerivedHide();
            b.ShowMessage();
            d.ShowMessage();
            h.ShowMessage();
            ((DerivedHide) h).ShowMessage();
        }
    }
    
    public class Base
    {
        public virtual void ShowMessage() => Console.WriteLine("I am Base.");
    }

    public class Derived : Base
    {
        // this is method overriding
        public override void ShowMessage() => Console.WriteLine("I am Derived.");
    }

    public class DerivedHide : Base
    {
        // this is method hiding
        public new void ShowMessage() => Console.WriteLine("I am DerivedHide.");
    }
}