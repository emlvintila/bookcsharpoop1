namespace Inheritance.Abstract
{
    public abstract class Shape
    {
        public abstract float Area { get; }
    }

    public class Square : Shape
    {
        public override float Area => Length * Length;
        public float Length { get; }

        public Square(float length) => Length = length;
    }
}