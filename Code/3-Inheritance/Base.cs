﻿namespace Inheritance.Base
{
    public class Rectangle
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public float Area => Width * Height;
        
        public Rectangle(float width, float height)
        {
            Width = width;
            Height = height;
        }
    }

    public class Square : Rectangle
    {
    	public Square(float length): base(length, length) {}
    }
}