using System;

namespace Introduction
{
    class Parsing
    {
        /*static*/ void Main(string[] args)
        {
            int age = 0;
            bool valid = false;
            while (valid == false)
            {
                Console.WriteLine("Please enter your age: ");
                string input = Console.ReadLine();
                valid = int.TryParse(input, out age);
            }

            Console.WriteLine("Your age is: {0}", age);
        }
    }
}
