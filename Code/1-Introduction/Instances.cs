﻿using System;

namespace Introduction
{
    class Instances
    {
        /*static*/ void Main(string[] args)
        {
            Rectangle rect = new Rectangle();
            rect.Width = 5;
            rect.Height = 6;
            Console.WriteLine("The area of rect is {0}.", rect.CalculateArea());
            
            Rectangle copy = rect;
            copy.Width = 7;
            Console.WriteLine("The area of rect is {0}.", rect.CalculateArea());
            Console.WriteLine("The area of copy is {0}.", copy.CalculateArea());
        }
    }

    class Rectangle
    {
        public float Width;
        public float Height;

        public float CalculateArea()
        {
            return Width * Height;
        }
    }
}
