using System;

namespace Introduction
{
    class Program
    {
        /*static*/ void Main(string[] args)
        {
            StaticExample instance1 = new StaticExample();
            StaticExample instance2 = new StaticExample();
            Console.WriteLine("How many StaticExamples have been instantiated? {0}", StaticExample.Count);
            // this property does not have a set accessor, so the following is invalid
            // StaticExample.Count = 0;
        }
    }

    class StaticExample
    {
        static int count;
        public static int Count => count;

        public StaticExample()
        {
            ++count;
        }
    }
}
