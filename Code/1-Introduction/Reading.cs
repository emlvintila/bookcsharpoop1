using System;

namespace Introduction
{
    class Reading
    {
        /*static*/ void Main(string[] args)
        {
            Console.WriteLine("What is your name? ");
            string name = Console.ReadLine();
            Console.WriteLine("Hello {0}!", name);
        }
    }
}