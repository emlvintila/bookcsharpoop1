using System;

namespace Introduction
{
    class This
    {
        static void Main(string[] args)
        {
            ThisExample instance = new ThisExample();
            Console.WriteLine("The value of number is: {0}", instance.GetNumber());
            instance.SetNumber(10);
            Console.WriteLine("The value of number is: {0}", instance.GetNumber());
            instance.SetNumberWithThis(10);
            Console.WriteLine("The value of number is: {0}", instance.GetNumber());
        }
    }

    class ThisExample
    {
        float number;

        public float GetNumber()
        {
            return number;
        }

        public void SetNumber(float number)
        {
            // incorrect, assignment was made to the number parameter
            number = number;
        }

        public void SetNumberWithThis(float number)
        {
            this.number = number;
        }
    }
}