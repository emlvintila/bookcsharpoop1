using System;

namespace Introduction
{
    class HelloWorld
    {
        /*static*/ void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
