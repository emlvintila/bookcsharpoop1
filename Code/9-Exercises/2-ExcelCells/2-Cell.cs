using System;

namespace Exercises.ExcelCells
{
    public class Cell<T>
    {
        private T value;

        public T Value
        {
            get => value;
            set
            {
                if (!value.Equals(this.value))
                {
                    this.value = value;
                    ValueChanged?.Invoke(value);
                }
            }
        }

        public event Action<T> ValueChanged;
    }

    public class ComputedCell<T> : Cell<T>
    {
        public ComputedCell(Cell<T> cell, Func<T, T> compute)
        {
            Value = compute(cell.Value);
            cell.ValueChanged += baseCellValue => Value = compute(baseCellValue);
        }
    }
}