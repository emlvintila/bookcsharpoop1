using NUnit.Framework;

namespace Exercises.ExcelCells
{
    [TestFixture]
    public class CellTests
    {
        [Test]
        public void TestCellEvent()
        {
            int sum = 0;
            Cell<int> cell = new Cell<int>();
            cell.ValueChanged += i => sum += i;
            cell.Value = 5;
            Assert.AreEqual(5, sum);
            cell.Value = 7;
            Assert.AreEqual(12, sum);
            cell.Value = 7;
            Assert.AreEqual(12 ,sum);
        }

        [Test]
        public void TestComputedCell()
        {
            Cell<int> cell = new Cell<int>();
            cell.Value = 2;
            ComputedCell<int> computedCell = new ComputedCell<int>(cell, i => i);
            Assert.AreEqual(cell.Value, computedCell.Value);
            cell.Value = 5;
            Assert.AreEqual(5, computedCell.Value);
            cell.Value = 17;
            Assert.AreEqual(17, computedCell.Value);
            
            ComputedCell<int> squared = new ComputedCell<int>(cell, i => i * i);
            cell.Value = 2;
            Assert.AreEqual(4, squared.Value);
        }

        [Test]
        public void TestComputedCellCascade()
        {
            Cell<int> cell = new Cell<int>();
            ComputedCell<int> middle = new ComputedCell<int>(cell, i => i + 1);
            ComputedCell<int> end = new ComputedCell<int>(middle, i => i + 1);
            cell.Value = 1;
            Assert.AreEqual(2, middle.Value);
            Assert.AreEqual(3, end.Value);
        }
    }
}