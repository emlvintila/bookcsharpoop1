using System;
using System.Collections;
using System.Collections.Generic;

namespace Exercises.ObservableCollection
{
    public class ObservableCollection<T> : IEnumerable<T>
    {
        private Node head;
        private Node tail;

        public IEnumerator<T> GetEnumerator()
        {
            for (Node current = head; !(current is null); current = current.Next)
                yield return current.Value;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public event CollectionChanged<T> CollectionChanged;

        public void Add(T item)
        {
            if (head is null)
                head = tail = new Node(item, null);
            else
                tail = tail.Next = new Node(item, null);
            CollectionChanged?.Invoke(this, new CollectionChangedEventArgs<T>(item, CollectionChangedEventType.Add));
        }

        public void Remove(T item)
        {
            for (Node current = head, prev = null; !(current is null); prev = current, current = current.Next)
                if (current.Value.Equals(item))
                {
                    if (!(prev is null))
                        prev.Next = current.Next;
                    else
                        head = head?.Next;

                    CollectionChanged?.Invoke(this,
                        new CollectionChangedEventArgs<T>(item, CollectionChangedEventType.Remove));
                    return;
                }
        }

        private class Node
        {
            public Node(T value, Node next)
            {
                Value = value;
                Next = next;
            }

            public T Value { get; set; }
            public Node Next { get; set; }
        }
    }

    public delegate void CollectionChanged<T>(ObservableCollection<T> collection, CollectionChangedEventArgs<T> args);

    public class CollectionChangedEventArgs<T>
    {
        public CollectionChangedEventArgs(T item, CollectionChangedEventType eventType)
        {
            Item = item;
            EventType = eventType;
        }

        public T Item { get; }
        public CollectionChangedEventType EventType { get; }
    }

    public enum CollectionChangedEventType
    {
        Add,
        Remove
    }
}