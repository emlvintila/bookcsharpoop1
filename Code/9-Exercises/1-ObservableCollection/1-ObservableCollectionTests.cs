using System.Collections.Generic;
using System.Linq;
using Exercises.ObservableCollection;
using NUnit.Framework;

namespace Exercises
{
    [TestFixture]
    public class ObservableCollectionTests
    {
        [Test]
        public void TestEvent()
        {
            ObservableCollection<int> collection = new ObservableCollection<int>();
            int addCount = 0;
            int addSum = 0;
            int removeCount = 0;
            int removeSum = 0;
            collection.CollectionChanged += (_, args) =>
            {
                if (args.EventType == CollectionChangedEventType.Add)
                {
                    ++addCount;
                    addSum += args.Item;
                }
                else
                {
                    ++removeCount;
                    removeSum += args.Item;
                }
            };
            collection.Add(1);
            Assert.AreEqual(1, addCount);
            Assert.AreEqual(1, addSum);
            Assert.AreEqual(0, removeCount);
            Assert.AreEqual(0, removeSum);
            collection.Add(41);
            Assert.AreEqual(2, addCount);
            Assert.AreEqual(42, addSum);
            Assert.AreEqual(0, removeCount);
            Assert.AreEqual(0, removeSum);
            collection.Remove(24);
            Assert.AreEqual(2, addCount);
            Assert.AreEqual(42, addSum);
            Assert.AreEqual(0, removeCount);
            Assert.AreEqual(0, removeSum);
            collection.Remove(41);
            Assert.AreEqual(2, addCount);
            Assert.AreEqual(42, addSum);
            Assert.AreEqual(1, removeCount);
            Assert.AreEqual(41, removeSum);
            collection.Remove(1);
            Assert.AreEqual(2, addCount);
            Assert.AreEqual(42, addSum);
            Assert.AreEqual(2, removeCount);
            Assert.AreEqual(42, removeSum);
        }

        [Test]
        public void TestIEnumerable()
        {
            IEnumerable<int> ints = new[] {1, 1, 2, 3, 4};
            ObservableCollection<int> collection = new ObservableCollection<int>();
            Assert.IsEmpty(collection);
            foreach (int i in ints)
                collection.Add(i);
            Assert.AreEqual(ints, collection);
            collection.Remove(1);
            Assert.AreEqual(ints.Skip(1), collection);
            collection.Remove(1);
            Assert.AreEqual(ints.Skip(2), collection);
            collection.Add(1);
            Assert.AreEqual(ints.Skip(2).Concat(new[] {1}), collection);
        }
    }
}