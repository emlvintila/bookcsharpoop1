﻿namespace Polymorphism.Interfaces
{
    public class Program
    {
        private /*static*/ void Main(string[] args)
        {
            Class instance = new Class();
            instance.Property = 42;
            // invalid, because of explicit interface implementation
            // instance.Method();
            IMethod method = instance;
            method.Method();
        }
    }

    public class Class : IProperty, IMethod
    {
        // implicit implementation
        public int Property { get; set; }

        // explicit implmentation
        void IMethod.Method()
        {
        }
    }

    public interface IProperty
    {
        int Property { get; set; }
    }

    public interface IMethod
    {
        void Method();
    }

    public interface IMethodNew : IMethod
    {
        new int Method();
    }
}