# Inheritance

Inheritance is the process through which a class, called the **derived class**, or **subtype**, **extends** another class, called the **base class**, or **supertype**. Through inheritance, the derived class inherits all members from the base class that **do not** have an access modifier of **private**.

Structs **cannot** inherit or be inherited.

In C#, `object` is the supertype of all types. Five of the methods it exposes are:

1. `bool Equals(object)` checks whether the current object is equal to the parameter
2. `static bool Equals(object, object)` checks whether the object instances are considered equal
3. `int GetHashCode()`  returns an `int` that serves at the object's hash for use in hash-based collections
4. `static bool ReferenceEquals(object, object)` checks whether the object instances refer to the same instance
5. `string ToString()` returns a string representation of the current object

The `Equals` method checks whether 2 objects have the same **value**, while the `ReferenceEquals` method checks whether 2 objects are actually the **same instance**.

```c#
object a = (object) 42;
object b = (object) 42;
// incorrect, use the Equals method
Console.WriteLine(a == b); // false
Console.WriteLine(Equals(a, b)); // true
Console.WriteLine(ReferenceEquals(a, b)); // false
```

## The base keyword

The `base` keyword is used inside derived classes to refer to members of the base class. In order to refer to the base constructor, **chain** the derived class constructor to the base class constructor by using the `base` keyword as a method (i.e. `base([arg, [arg...]])`); this is only valid when declaring a derived class constructor.

```c#
namespace Inheritance.Base
{
    public class Rectangle
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public float Area => Width * Height;
        
        public Rectangle(float width, float height)
        {
            Width = width;
            Height = height;
        }
    }

    public class Square : Rectangle
    {
    	public Square(float length) : base(length, length) { }
    }
}
```

**Constructor chaining** can also be used to chain to another constructor of the same class, using the `this` keyword, instead of the `base` keyword. The following is essentially identical to the `Square` constructor, but it is just a shortcut; the 2 parameter constructor is still accessible and both of them can be used. Constructor chaining is useful for code reuse.

```c#
public Rectangle(float length) : this(length, length) { }
```

## The virtual, abstract, override keywords

### virtual and override

The `virtual` keyword can be used on methods and properties. **Only** members marked as virtual **can be overridden** in derived classes. The `override` keyword specifies a declaration of an overridden member, and subsequent derived classes can also override the respective member. An overridden member must have the same accessibility modifier as its virtual ancestor.

```c#
using System;

namespace Inheritance.Virtual
{
    public class Porgram
    {
        private static void Main(string[] args)
        {
            Base b = new Base();
            Base d = new Derived();
            Base h = new DerivedHide();
            b.ShowMessage();
            d.ShowMessage();
            h.ShowMessage();
            ((DerivedHide) h).ShowMessage();
        }
    }
    
    public class Base
    {
        public virtual void ShowMessage() => Console.WriteLine("I am Base.");
    }

    public class Derived : Base
    {
        // this is method overriding
        public override void ShowMessage() => Console.WriteLine("I am Derived.");
    }

    public class DerivedHide : Base
    {
        // this is method hiding
        public new void ShowMessage() => Console.WriteLine("I am DerivedHide.");
    }
}
```

#### member hiding

If a member declared in a derived class has the same signature as one in the base class, the derived class member **hides** it. The compiler warns about hiding an inherited member. If hiding was **intended**, then the programmer should use the `new` keyword before the return type of the method. 

#### Liskov substitution principle

Substitutability is a principle in object-oriented programming stating that, in a computer program, if S is a subtype of T, then objects of type T may be replaced with objects of type S (i.e. an object of type T may be substituted with any object of a subtype S) without altering any of the desirable properties of the program (correctness, task performed, etc.). More formally, the [Liskov substitution principle](https://en.wikipedia.org/wiki/Liskov_substitution_principle) (LSP) is a particular definition of a subtyping relation, called (strong) behavioral subtyping.

> Subtype Requirement: Let $\phi (x)$ be a property provable about objects $x$ of type T. Then $\phi (y)$ should be true for objects $y$ of type S where S is a subtype of T.

In the above example, all 3 instances of classes are typed as `Base`, but 2 of them hold references to derived types. The Liskov substitution principle allows the program be well-formed and syntactically correct.

#### Dynamic method dispatch and the virtual method table

When calling the `ShowMessage` method on the objects in the example, the method is looked up in the **[virtual method table](https://en.wikipedia.org/wiki/Virtual_method_table)** at **runtime**. Because the objects are typed as `Base`, the call `h.ShowMessage` resolved to `Base.ShowMessage`, because there is no virtual `ShowMessage` method on the `DerivedHide` class. This process is called **[dynamic dispatch](https://en.wikipedia.org/wiki/Dynamic_dispatch)**. 

Finally, the **cast** to `DerivedHide` along with the call to `ShowMessage` shows the 'expected' message, because there is no virtual method lookup, the call was **statically dispatched** at compile-time.

### abstract

The `abstract` keyword can be used on classes, methods, and properties. An abstract class **cannot be instantiated** regardless of whether it declares an accessible constructor.

An abstract member is implicitly virtual, but it cannot define an implementation. A derived class that extends a base class with abstract members, **must** provide the implementation for all the abstract members.

```c#
namespace Inheritance.Abstract
{
    public abstract class Shape
    {
        public abstract float Area { get; }
    }

    public class Square : Shape
    {
        public override float Area => Length * Length;
        public float Length { get; }

        public Square(float length) => Length = length;
    }
}
```

### sealed

The `sealed` keyword can only be used on classes. A sealed class **cannot** be inherited.

## The open-closed principle

The [open-closed principle](https://en.wikipedia.org/wiki/Open%E2%80%93closed_principle) states "software entities should be open for extension, but closed for modification"; that is, such an entity can allow its behavior to be extended without modifying its source code.

> - A module will be said to be open if it is still available for extension. For example, it should be possible to add fields to the data structures it contains, or new elements to the set of functions it performs.
> - A module will be said to be closed if it is available for use by other modules. This assumes that the module has been given a well-defined, stable description (the interface in the sense of information hiding).

### Extension methods

Because a sealed class cannot be inherited, the programmer cannot add new members to a sealed class. If the required functionality can be achieved by using the already defined class members, the programmer may define an 'utility method' that takes an instance of the respective class as a parameter.

For example, assuming the `Point` class is defined in another assembly:

```c#
public sealed class Point
{
    public float X { get; set; }
    public float Y { get; set; }
    /* ... */
}

public static class PointUtilities
{
    public static float GetMaxCoordinate(Point point) => Math.Max(point.X, point.Y);
}

Point point = new Point(2, 3);
Console.WriteLine(PointUtilities.GetMaxCoordinate(point)); // prints 3
```

The `GetMaxCoordinate` method can be defined as an **extension method** instead. An extension method must be a static method, declared in a static class. The `this` keyword is added to the first parameter it takes, thus turning the method in an extension method. An extension method can be invoked like an instance method, but it **does not have access** to **protected members**. The above method, rewritten as an extension method:

```c#
public static class PointExtensions
{
    // note the this modifier in the parameter declaration
    public static float GetMaxCoordinate(this Point point) => Math.Max(point.X, point.Y);
}

Point point = new Point(2, 3);
// the method is invoked like an instance method
Console.WriteLine(point.GetMaxCoordinate()); // prints 3
```

## The diamond problem

Consider the following scenario, where the `Copier` class must inherit from both `Scanner` and `Printer`.

![The diamond problem](../Images/3-Diamond.png)

Assuming the above scenario, a simple implementation would be the following:

```c#
public class PoweredDevice { }
public class Scanner : PoweredDevice
{
	public void Start() { }
}
public class Printer : PoweredDevice
{
	public void Start() { }
}
public class Copier : Scanner, Printer
{
}
```

But C# does not support multiple inheritance. Which `Start` method should `Copier` inherit? It cannot inherit both. 

#### Inheritance versus composition

The solution to the 'diamond problem' is simple. Instead of inheritance, use **composition** and **delegation**.

```c#
public class PoweredDevice { }
public class Scanner : PoweredDevice
{
	public void Start() { }
}
public class Printer : PoweredDevice
{
	public void Start() { }
}
public class Copier
{
	private Scanner scanner;
	private Printer printer;
    
    public Copier(Scanner scanner, Printer printer)
    {
        this.scanner = scanner;
        this.printer = printer;
    }
    
    public void Start()
    {
        scanner.start();
        printer.Start();
    }
}
```

The `Copier` class now contains a `Scanner` instance and a `Printer` instance. It **delegates** the `Start` method to both the `Printer` and the `Scanner`.

