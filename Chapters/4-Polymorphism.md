# Polymorphism

## Interfaces

In C#, an interface declaration is similar to a class declaration. Interfaces can only declare methods and properties. All members declared by an interface are implicitly **public abstract**. Interfaces are declared using the `interface` keyword.

*<u>Code style</u>: Interface names start with an 'I', and don't end in 'Interface'.*

```c#
namespace Polymorphism.Interfaces
{
    public interface IProperty
    {
        int Property { get; set; }
    }

    public interface IMethod
    {
        void Method();
    }

    public interface IMethodNew : IMethod
    {
        new int Method();
    }
}
```

An interface can extend another interface. The only modifier that can be applied to an interface member declaration is `new`, when member hiding is intended by the extending interface.

### Implementing interfaces

Classes and structs can **implement** an arbitrary number of interfaces. Implementing interfaces is different from extending classes, because the implementing class **does not inherit** the interface methods. There are two types of interface implementation, implicit and explicit. Explicit implementation is useful if the class implements multiple interfaces that expose members with colliding signatures.

```c#
namespace Polymorphism.Interfaces
{
    public class Program
    {
        private static void Main(string[] args)
        {
            Class instance = new Class();
            instance.Property = 42;
            // invalid, because of explicit interface implementation
            // instance.Method();
            IMethod method = instance;
            method.Method();
        }
    }

    public class Class : IProperty, IMethod
    {
        // implicit implementation
        public int Property { get; set; }

        // explicit implmentation
        void IMethod.Method() { }
    }
}
```

*<u>Code style</u>: Implement interfaces implicitly when possible.*

## The interface segregation principle

The [interface segregation principle](https://en.wikipedia.org/wiki/Interface_segregation_principle) (ISP) states that no client should be forced to depend on methods it does not use. ISP splits interfaces that are very large into smaller and more specific ones so that clients will only have to know about the methods that are of interest to them. Such shrunken interfaces are also called **role interfaces**.

## The is and as operators

The `is` operator is used to check whether an object instance is of a specific type.

The `<expr> is <type>` statement is true if:

- `<expr>` is an instance of the same type as `<type>`.
- `<expr>` is an instance of a type that derives from *type*. In other words, the result of `<expr>` can be upcast to an instance of `<type>`.
- `<expr>` has a compile-time type that is a base class of *type*, and `<expr>` has a runtime type that is `<type>` or is derived from `<type>`. The **compile-time type** of a variable is the variable's type as defined in its declaration. The **runtime type** of a variable is the type of the instance that is assigned to that variable.
- `<expr>` is an instance of a type that implements the `<type>` interface.

The `as` operator casts an object instance to a specific type. The type to cast into **must** be a reference type. If the cast is not valid, it returns **null** instead of throwing an exception.

The following example assumes the previous example's declarations:

```c#
object instance = new Class();
if (instance is IProperty)
{
    // the result will never be null, because of the check above
    IProperty obj = instance as IProperty;
    // the above and below casts are equivalent
    // IProperty obj = (IProperty) instance;
    obj.Property = 42;
}
IMethodNew methodNew = instance as IMethodNew;
if (methodNew == null)
    Console.WriteLine("The object instance does not implement the IMethodNew interface.");
```

There is another syntax for using the `is` operator, called **pattern matching**, which removes the need of casting.

```c#
object instance = new Class();
if (instance is IProperty obj)
    obj.Property = 42;
```

The `is` operator can also be used to check whether an object is equal to a constant.

```c#
const int HIGH_ROLL = 6;
int roll = new Random().Next(1, 7);
if (roll is HIGH_ROLL)
    Console.WriteLine("The roll is a high roll.");
object obj = null;
if (obj is null)
    Console.WriteLine("obj is null.");
```

