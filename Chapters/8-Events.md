# Event-driven programming

[Event-driven programming](https://en.wikipedia.org/wiki/Event-driven_programming) is a programming paradigm in which the flow of the program is determined by events such as user actions (mouse clicks, key presses), sensor outputs, or messages from other programs or threads.

## The inversion of control principle

In software engineering, [inversion of control](https://en.wikipedia.org/wiki/Inversion_of_control) (IoC) is a programming principle. IoC inverts the flow of control as compared to traditional control flow. In IoC, custom-written portions of a computer program receive the flow of control from a generic framework. Inversion of control is used to increase modularity of the program and make it extensible.

## Delegates

### Delegate declaration

A delegate is a special type that represents a method by its signature and return type. A delegate instance is a reference to a specific method that matches the delegate's signature.

A delegate type is declared using the `delegate` keyword. Because it is a type, it may be delcared directly inside a namespace. A delegate does not have a 'method body', because it is not a method, it is only a reference to a method. Methods are implicitly convertible to delegates with matching signatures.

```
[<access_modifier>] delegate <return_type> <delegate_name>([<parameter_list>]);
```

```c#
public delegate void Action();
```

There are multiple delegate declarations in the `System` namespace:

- An `Action` is a delegate that takes no parameters and returns no value. An `Action<T1>` takes one parameter of type `T1` and returns no value, etc.
- A `Func<TOut>` is a delegate that takes no parameters and returns a value of type `TOut`. A `Func<T1, TOut>` takes one parameter of type `T1` and returns a value of type `TOut`, etc.

### Using delegates

```c#
public void DoSomething() { Console.WriteLine("Write something to the console."); }
// note the missing (), the DoSomething method is not called here
// its reference is assigned to the action variable
Action action = DoSomething; // action is a delegate instance
// the following calls are equivalent
action();
action.Invoke();
```

### Anonymous delegates

An anonymous delegate creates an anonymous method that can be assigned to a delegate instance. The **parameter list can be omitted** if the parameters are not used inside the method.

```
delegate [(<parameter_list>)] { <statements> };
```

```c#
Action anon = delegate { Console.WriteLine("Writing from an anonymous delegate."); };
Action<int> anon2 = delegate { Console.WriteLine("The parameter list was omitted."); };
anon.Invoke();
anon2.Invoke();
```

### Delegate addition and subtraction

Multiple delegates of the same type can be combined into one delegate using the addition operator `+`.

```c#
Action a = delegate { System.Console.WriteLine("A"); };
Action b = delegate { System.Console.WriteLine("B"); };
Action ab = a + b;
ab.Invoke();
Action aba = ab + a;
aba.Invoke();
```

Delegates can be subtracted using the subtraction operator `-`. The subtraction operator behaves differently than the addition operator, as it attempts to remove a list from another list.

```c#
Action a = delegate { System.Console.WriteLine("A"); };
Action a = delegate { System.Console.WriteLine("B"); };
Action a = delegate { System.Console.WriteLine("C"); };
Action s = a + b + c;
s.Invoke();                  // ABC
(s - a).Invoke();            // BC
(s - b).Invoke();            // AC
(s - c).Invoke();            // AB
(s - (a + b)).Invoke();      // C
(s - (b + c)).Invoke();      // A
// a + c is not a sub-list of a + b + c
(s - (a + c)).Invoke();      // ABC
```

The right-hand side expression **must be a sub-list** of delegates that is present in the left-hand side expression. 

Source: [JetBrains Rider – Code Inspection: Delegate subtractions](https://www.jetbrains.com/help/rider/DelegateSubtraction.html)

### Lambda expressions

A lambda expression is a short-hand syntax for creating anonymous methods. Specifying the return type is not required, and **parameter types can be omitted**. Lambda expressions are implictly assignable to delegates. If there is exactly one parameter, the parentheses may also be omitted.

```
(<parameter_list>) => <expression_or_statement>;
```

There is also a long syntax for creating 'statement lambdas'.

```
(<parameter_list>) => { <statements> };
```

```c#
Action a = () => Console.WriteLine("Lambda expression.");
Func<int, int> square = x => x * x;
Func<int, int, int> add = (x, y) => x + y;
Action statementLambda = () => { Console.WriteLine("Statement lambda."); };
```

Note that the `add` function defined above cannot be [partially-applied](https://en.wikipedia.org/wiki/Partial_application), because it takes 2 arguments, not 1. In order to define a [curry-able](https://en.wikipedia.org/wiki/Currying) add function, you must modify its type to `Func<int, Func<int, int>>`. It reads as: "A function that takes an `int` and returns a function that takes an `int` and returns an `int`". The process is tedious for functions of higher [arity](https://en.wikipedia.org/wiki/Arity), and usually not neccessary, as C#'s primary paradigm is OOP, not FP (functional programming).

```c#
Func<int, Func<int, int>> add = x => y => x + y;
Func<int, Func<int, Func<int, int>>> multiplyThenAdd = x => y => z => x * y + z;
```

### Closures

Both anonymous delegates and lambda expressions create a **closure**. They **capture** ('close over', hence the name of closure) the local variables that are used inside their bodies **by reference**.

```c#
int x = 4;
Action a = () => Console.WriteLine(x);
a.Invoke(); // prints 4
x = 2;
a.Invoke(); // prints 2, because the value of x was modified
```

## Events

An event is an occurrence that is localized at a single point in time. For example, an event might represent adding an item to a list.

In C#, an event is declared using the `event` keyword, and it represents a **list of delegates**, called **event handlers**, that will be invoked when the event occurs. In a part of the program, some code **subscribes** a number of event handlers for the event. The event is then **triggered** **inside the class that declared the event**; an event can only be triggered by the declaring class.

The `+=` operator is used to subscribe to an event. The `-=` operator is used to unsubscribe from an event.

### Event declaration

Because an event represents a list of delegates, it must be declared using a delegate type.

```
[<access_modifier>] event <delegate_type> <event_name>;
```

```c#
public event System.EventHandler SomeEvent;
```

### Using events

```c#
using System;

namespace Events.UsingEvents
{
    public class Program
    {
        private static void Main(string[] args)
        {
            List list = new List();
            list.ItemAdded += ListOnItemAdded;
            list.Add(42);
            list.ItemAdded -= ListOnItemAdded;
            list.Add(24);
            // the following calls are invalid, because they are not inside the List class
            // list.ItemAdded(15);
            // list.ItemAdded.Invoke(51);
        }

        private static void ListOnItemAdded(List list, object newitem)
        {
            Console.WriteLine("{0} was added to the list.", newitem);
        }
    }

    public class List
    {
        public delegate void ItemAddedDelegate(List list, object newItem);
        
        public event ItemAddedDelegate ItemAdded;

        public void Add(object obj)
        {
            if (!(ItemAdded is null))
                ItemAdded.Invoke(this, obj);
        }
    }
}
```

## The observer pattern

The [observer pattern](https://en.wikipedia.org/wiki/Observer_pattern) is a design pattern in which an object, called the **subject**, maintains a list of its dependents, called **observers**, and notifies them automatically of any state changes, usually by calling one of their methods.

### The IObservable\<T\> and IObserver\<T\> interfaces

#### The IObservable\<T\> interface

The `IObservable<T>` interface is implemented by the subject, and it exposes the `Subscribe` method that allows an `IObserver<T>` to subscribe to state changes.

#### The IObserver\<T\> interface

The `IObserver<T>` interface is implemented by the observer; it exposes the `OnNext`, `OnError`, and `OnCompleted` methods that should be called by the subject.

- The `OnNext` method should be called when there are new changes to be processed, with the argument representing the change in state;
- The `OnError` method should be called with an `Exception` argument that represents an error that the subject encountered;
- The `OnCompleted` method should be called when the subject has finished sending messages. There should be no more calls to `OnNext` or `OnError` after a call to `OnCompleted`.

The following example is similar to the example above, but it uses the observer pattern instead.

```c#
using System;
using System.Collections.Generic;

namespace Events.ObserverPattern
{
    public class Program
    {
        private static void Main(string[] args)
        {
            List<int> list = new List<int>();
            Observer<int> observer = new Observer<int>();
            // the following are equivalent
            // IDisposable unsubscriber =  observer.Register(list);
            IDisposable unsubscriber = list.Subscribe(observer);
            
            list.Add(42);
            unsubscriber.Dispose();
            list.Add(24);
        }
    }
    
    public class List<T> : IObservable<T>
    {
        public List() => Observers = new System.Collections.Generic.List<IObserver<T>>();

        private IList<IObserver<T>> Observers { get; }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (!Observers.Contains(observer))
                Observers.Add(observer);
            return new Unsubscriber<T>(Observers, observer);
        }

        public void Add(T item)
        {
            foreach (IObserver<T> observer in Observers)
                observer.OnNext(item);
        }
    }

    public class Unsubscriber<T> : IDisposable
    {
        private readonly IObserver<T> observer;
        private readonly IList<IObserver<T>> observers;

        public Unsubscriber(IList<IObserver<T>> observers, IObserver<T> observer)
        {
            this.observers = observers;
            this.observer = observer;
        }

        public void Dispose()
        {
            if (!(observer is null) && observers.Contains(observer))
                observers.Remove(observer);
        }

        ~Unsubscriber() => Dispose();
    }

    public class Observer<T> : IObserver<T>
    {
        public void OnCompleted() { }

        public void OnError(Exception error) { }

        public void OnNext(T value) => Console.WriteLine(value);

        public IDisposable Register(List<T> list) => list.Subscribe(this);
    }
}
```

