# Exercises

All of the following exercises require the understanding of generic types and generic type constraints.

## 1. Stack

Implement a `Stack<T>` with the `Push`, `Pop`, `IsEmpty` methods, and a `Count` property. The stack should resize dynamically when more elements are added to it. Hint: If you are going to use an array for the implementation, the `Array.Copy` method might be useful.

This exercise demonstrates the basic use of generic types.

### Starting code

```c#
namespace Exercises.Stack
{
    public class Stack<T>
    {
        public int Count { get; }
        
        public void Push(T element) { }
        
        public T Pop() { }
        
        public bool IsEmpty() { }
    }
}
```

### Test code

```c#
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Exercises.Stack
{
    [TestFixture]
    public class StackTests
    {
        [Test]
        public void TestCount()
        {
            IEnumerable<int> ints = new[] {1, 2, 3};
            Stack<int> stack = new Stack<int>();
            foreach (int i in ints)
                stack.Push(i);
            
            Assert.AreEqual(ints.Count(), stack.Count);
        }

        [Test]
        public void TestEmpty()
        {
            Stack<int> stack = new Stack<int>();
            Assert.IsTrue(stack.IsEmpty());
            stack.Push(42);
            Assert.IsFalse(stack.IsEmpty());
        }

        [Test]
        public void TestEmptyPopThrows()
        {
            Stack<int> stack = new Stack<int>();
            Assert.Throws<InvalidOperationException>(() => stack.Pop());
        }

        [Test]
        public void TestLifo()
        {
            IEnumerable<int> ints = new[] {1, 2, 3, 4, 5};
            Stack<int> stack = new Stack<int>();
            foreach (int i in ints)
                stack.Push(i);
            List<int> popped = new List<int>(ints.Count());
            while (!stack.IsEmpty())
                popped.Add(stack.Pop());
            
            Assert.AreEqual(popped, ints.Reverse());
        }

        [Test]
        public void TestBigSize()
        {
            const int size = 1000;
            Stack<int> stack = new Stack<int>();
            foreach (int i in Enumerable.Range(0, size))
                stack.Push(i);
            
            Assert.AreEqual(stack.Count, size);
        }
    }
}
```

### Reference implementation

```c#
using System;

namespace Exercises.Stack
{
    public class Stack<T>
    {
        private const int CAPACITY_INITIAL = 20;
        private const int CAPACITY_DELTA = 20;
        private int current;
        private T[] elements;

        public Stack()
        {
            current = -1;
            elements = new T[CAPACITY_INITIAL];
        }

        public int Count => current + 1;

        public void Push(T element)
        {
            if (current == elements.Length - 1)
            {
                T[] newElements = new T[elements.Length + CAPACITY_DELTA];
                Array.Copy(elements, newElements, elements.Length);
                elements = newElements;
            }

            elements[++current] = element;
        }

        public T Pop()
        {
            if (IsEmpty())
                throw new InvalidOperationException();
            T element = elements[current];
            elements[current] = default;
            --current;
            
            return element;
        }

        public bool IsEmpty() => current < 0;
    }
}
```

## 2. Linked list

Implement a `LinkedList<T>` without using a backing collection for the elements. The linked list must implement the `IEnumerable<T>` interface, and must define the `Add` and `Remove` methods.

This exercise demonstrates the use of nested classes.

### Starting code

```c#
using System.Collections.Generic;

namespace Exercises.LinkedList
{
    public class LinkedList<T> : IEnumerable<T>
    {
        private class Node
        {
            public T Value { get; set; }
            public Node Next { get; set; }
        }
        
        public void Add(T value) { }
        
        public void Remove(T value) { }
    }
}
```

### Test code

```c#
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Exercises.LinkedList
{
    [TestFixture]
    public class LinkedListTests
    {
        [Test]
        public void Test()
        {
            IEnumerable<int> ints = new[] {1, 2, 3, 4, 5};
            LinkedList<int> list = new LinkedList<int>();
            Assert.IsEmpty(list);
            foreach (int i in ints)
                list.Add(i);
            Assert.AreEqual(ints, list);
            list.Remove(ints.First());
            Assert.AreEqual(ints.Skip(1), list);
            list.Remove(ints.Skip(1).First());
            Assert.AreEqual(ints.Skip(2), list);
            foreach (int i in ints)
                list.Remove(i);
            Assert.IsEmpty(list);
        }
    }
}
```

### Reference implementation

```c#
using System.Collections;
using System.Collections.Generic;

namespace Exercises.LinkedList
{
    public class LinkedList<T> : IEnumerable<T>
    {
        private Node head;
        
        private class Node
        {
            public T Value { get; set; }
            public Node Next { get; set; }
            
            public Node(T value, Node next)
            {
                Value = value;
                Next = next;
            }
        }

        public void Add(T value)
        {
            if (head is null)
                head = new Node(value, null);
            else
            {
                Node current = head;
                while (!(current.Next is null))
                    current = current.Next;
                current.Next = new Node(value, null);
            }
        }

        public void Remove(T value)
        {
            for (Node current = head, prev = null; !(current is null); 
            	prev = current, current = current.Next)
                if (current.Value.Equals(value))
                {
                    if (prev != null)
                        prev.Next = current.Next;
                    else if (!(head is null))
                        head = head.Next;
                    return;
                }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (Node current = head; !(current is null); current = current.Next)
                yield return current.Value;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
```

## 3. Sorted set

Implement a `SortedSet<T>` collection; it must keep the elements in ascending order, and must not contain duplicate elements. The set must resize dynamically when more elements are added to it.

It must implement the `IEnumerable<T>` interface, and must define the `Add`, `Remove`, and `IndexOf` methods, and the `Count` property. It is not required to handle `null` values.

This exercise demonstrates the use of generic type constraints.

### The IComparable\<T\> interface

The `IComparable<T>` interface exposes a `CompareTo(T other)` method that returns an integer value. If the value is lower than zero, the object comes before `other` in the sort order. If the value is higher than zero, the object comes after `other` in the sort order. Otherwise, their sort order is the same, which **usually** (not necessarily always) means that the object values are equal.

### Starting code

```c#
using System;
using System.Collections.Generic;

namespace Exercises.SortedSet
{
    public class SortedSet<T> : IEnumerable<T>
        where T : IComparable<T>
    {
        private T[] elements;
        
        public void Add(T element) { }
        
        public void Remove(T element) { }
        
        public int IndexOf(T element) { }
    }
}
```

### Test code

```c#
using System.Collections.Generic;
using NUnit.Framework;

namespace Exercises.SortedSet
{
    [TestFixture]
    public class SortedSetTests
    {
        [Test]
        public void TestAdd()
        {
            IEnumerable<int> ints = new[] {4, 1, 3, 7};
            IEnumerable<int> expected = new[] {1, 3, 4, 7};
            SortedSet<int> set = new SortedSet<int>();
            foreach (int i in ints)
                set.Add(i);

            Assert.AreEqual(expected, set);
        }

        [Test]
        public void TestCount()
        {
            SortedSet<int> set = new SortedSet<int>();
            for (int i = 1; i <= 5; ++i)
                for (int j = 1; j <= 5; ++j)
                {
                    set.Add(i);
                    Assert.AreEqual(i, set.Count);
                }
        }

        [Test]
        public void TestIndexOf()
        {
            IEnumerable<int> ints = new[] {7, 6, 5, 4};
            SortedSet<int> set = new SortedSet<int>();
            foreach (int i in ints)
                set.Add(i);
            Assert.AreEqual(0, set.IndexOf(4));
            Assert.AreEqual(1, set.IndexOf(5));
            Assert.Less(set.IndexOf(42), 0);
        }

        [Test]
        public void TestRemove()
        {
            IEnumerable<int> ints = new[] {3, 2, 5, 8};
            SortedSet<int> set = new SortedSet<int>();
            foreach (int i in ints)
                set.Add(i);
            set.Remove(3);
            Assert.AreEqual(new[] {2, 5, 8}, set);
            for (int i = 1; i <= 2; ++i)
            {
                set.Remove(2);
                Assert.AreEqual(new[] {5, 8}, set);
            }

            set.Remove(8);
            Assert.AreEqual(new[] {5}, set);
            set.Remove(5);
            Assert.IsEmpty(set);
        }
    }
}
```

### Reference implementation

This implementation is efficient because it uses the sorted property of the collection, and uses binary search for the operations, therefore making their complexity **O(log(n))**.

```c#
using System;
using System.Collections;
using System.Collections.Generic;

namespace Exercises.SortedSet
{
    public class SortedSet<T> : IEnumerable<T>
        where T : IComparable<T>
    {
        private const int CAPACITY_INITIAL = 20;
        private const int CAPACITY_DELTA = 20;
        private int current;
        private T[] elements;

        public SortedSet()
        {
            elements = new T[CAPACITY_INITIAL];
            current = -1;
        }

        public int Count => current + 1;

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i <= current; ++i)
                yield return elements[i];
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public void Add(T element)
        {
            if (current == elements.Length - 1)
            {
                T[] newElements = new T[elements.Length + CAPACITY_DELTA];
                Array.Copy(elements, newElements, elements.Length);
                elements = newElements;
            }

            switch (Count)
            {
                case 0:
                    elements[++current] = element;
                    return;
                case 1:
                    int compare = element.CompareTo(elements[0]);
                    if (compare < 0)
                    {
                        ShiftRight(0);
                        elements[current++] = element;
                    }
                    else if (compare > 0)
                    {
                        elements[++current] = element;
                    }

                    return;
                default:
                    if (element.CompareTo(elements[current]) > 0)
                    {
                        elements[++current] = element;
                        return;
                    }

                    if (element.CompareTo(elements[0]) < 0)
                    {
                        ShiftRight(0);
                        elements[0] = element;
                        ++current;
                        return;
                    }

                    for (int low = 0, high = current; low <= high;)
                    {
                        int middle = (low + high) / 2;
                        int next = middle + 1;

                        if (elements[middle].CompareTo(element) < 0)
                        {
                            if (elements[next].CompareTo(element) > 0)
                            {
                                ShiftRight(next);
                                elements[next] = element;
                                ++current;
                                return;
                            }

                            low = middle + 1;
                        }
                        else
                        {
                            high = middle - 1;
                        }
                    }

                    return;
            }
        }

        public void Remove(T element)
        {
            int index = IndexOf(element);
            if (index == -1)
                return;

            ShiftLeft(index);
            elements[current] = default;
            --current;
        }

        public int IndexOf(T element)
        {
            for (int low = 0, high = current; low <= high;)
            {
                int middle = (low + high) / 2;
                int compare = element.CompareTo(elements[middle]);
                if (compare == 0)
                    return middle;
                if (compare < 0)
                    high = middle - 1;
                else
                    low = middle + 1;
            }

            return -1;
        }

        private void ShiftLeft(int from)
        {
            for (int i = from; i < current; ++i)
                elements[i] = elements[i + 1];
        }

        private void ShiftRight(int from)
        {
            for (int i = current; i >= from; --i)
                elements[i + 1] = elements[i];
        }
    }
}
```

