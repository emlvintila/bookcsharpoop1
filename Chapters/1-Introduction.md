# Introduction

This book aims to teach the reader about the basics of the object oriented programming (OOP) paradigm, as well as the C# language.

It is assumed that the reader has basic knowledge about data structures, algorithms, and mathematics. The purpose of the book is not to teach data structures or algorithms, but use the reader's knowledge about them in order to implement them in an object oriented manner in the C# language.

All the provided source code was compiled at the `C# 8.0` language level targeting `.NET Core 3.0`.

## The C# language

### Object oriented programming (OOP)

In the object oriented programming, every entity within a program is an object. An object has data associated with it, in the form of **fields** or **properties**. Besides containing data, an object can also contain code in the form of procedures that help it carry out its responsibility. These procedures are part of the object, and they are called **methods**. Collectively, fields, properties, and methods, are called **members** of the respective object.

C# supports the following types:

1. **classes**, declared using the `class` keyword
2. **interfaces**, declared using the `interface` keyword
3. **structures**, declared using the `struct` keyword
4. **enumerations**, declared using the `enum` keyword.

For the introduction only classes will be used.

*Note: The word 'type' is used to refer to classes, interfaces, structures and enumerations collectively. In order to avoid any confusion, the word 'class' will be used further on, but remember that it can be substituted with struct, and for the most part, with interface as well.*

In C#, code cannot appear outside of a class declaration; any field, property, or method **must** be a member of a class. 

#### The single responsibility principle

The [**single responsibility principle**](https://en.wikipedia.org/wiki/Single_responsibility_principle) states every class or method should have responsibility over a single part of the functionality provided by the software, and that responsibility should be entirely encapsulated by the class.

#### The three 'pillars' of OOP

*Each of the following concepts will be presented more thoroughly separately, in their own chapter further on.*

##### Inheritance

Inheritance is the concept of basing a class upon another class, inheriting the fields, properties, and methods of the base class, and allowing the derived class to define new behavior, or **override** the base class's behavior.

C# does not support multiple inheritance; a class cannot inherit from multiple classes. `System.Object` is the ultimate base class in C#, every class inherits from it implicitly.

##### Encapsulation

Encapsulation is a language mechanism for restricting direct access to some of the class's members.

In C#, encapsulation is achieved through the use of access modifiers. By default, any member of a class can only be accessed by code inside the class.

*Note: For the introduction all members will be marked as public to avoid confusion.*

##### Polymorphism

Subtype polymorphism (subtyping) allows a method to be written to take an object of a certain type *T*, but also work correctly, if passed an object that belongs to a type *S* that is a subtype of *T*. This type relation is sometimes written $S <: T$. Conversely, *T* is said to be a *supertype* of *S*—written $T :> S$.

While C# does not support multiple inheritance, it does support classes implementing an arbitrary number of interfaces.

### Data types

The `.NET Framework` has a multitude of pre-defined types, classes, structures, interfaces, and enumerations. The following table presents the **simple types** in C#.

| C# type   | .NET type        | Description                                          |
| --------- | ---------------- | ---------------------------------------------------- |
| `object`  | `System.Object`  | An `object` can hold a value of any type             |
| `bool`    | `System.Boolean` | Logical value that can hold either `false` or `true` |
| `sbyte`   | `System.Int8`    | 8 bit signed integer $[-2^{7}, 2^{7} - 1]$           |
| `byte`    | `System.UInt8`   | 8 bit unsigned integer $[0, 2^{8} - 1)]$             |
| `short`   | `System.Int16`   | 16 bit signed integer $[-2^{15}, 2^{15} - 1]$        |
| `ushort`  | `System.UInt16`  | 16 bit unsigned integer $[0, 2^{16} - 1]$            |
| `int`     | `System.Int32`   | 32 bit signed integer $[-2^{31}, 2^{31} - 1]$        |
| `uint`    | `System.UInt32`  | 32 bit unsigned integer $[0, 2^{32} - 1]$            |
| `long`    | `System.Int64`   | 64 bit signed integer $[-2^{63}, 2^{63} - 1]$        |
| `ulong`   | `System.UInt64`  | 64 bit unsigned integer $[0, 2^{64} - 1]$            |
| `decimal` | `System.Decimal` | 128 bit signed floating-point number                 |
| `float`   | `System.Single`  | IEEE754 single precision floating-point number       |
| `double`  | `System.Double`  | IEEE754 double precision floating-point number       |
| `char`    | `System.Char`    | Represents a character as a UTF-16 code unit         |
| `string`  | `System.String`  | Represents text as a sequence of UTF-16 code units   |

*Note: Keep in mind, of the above types, only `object` and `string`  are reference types, the rest are all value types.*

*<u>Code style</u>: Use the type aliases for standard `.NET` types.*

#### Default values

- numeric types = 0
- bool = false
- value types (structs) = an instance with its members initialized to their default values
- reference types = null

*<u>Code style</u>: Do not initialize fields or properties with their default values.*

#### Arrays

Array are declared using the type name followed by `[]`. When creating a new array, the size **must** be specified inside the brackets. Arrays are reference types.

```c#
int[] array; // = null by default
// int[] vector = new int[]; // invalid, must specify the size
int[] tenInts = new int[10];
```

### Operators

The arithmetic operators cannot be mixed with bitwise and logic operators. The operators with the lowest priority are evaluated **first**.

#### Arithmetic operators

| Operator | Priority | Example      |
| -------- | -------- | ------------ |
| $*$      | 0        | $6 * 7 = 42$ |
| $/$      | 0        | $5 / 2 = 2$  |
| $\%$     | 0        | $5 \% 2 = 1$ |
| $+$      | 1        | $1 + 2 = 3$  |
| $-$      | 1        | $4 - 5 = -1$ |

#### Bitwise operators

| Operator | Priority | Example          |
| -------- | -------- | ---------------- |
| $\sim{}$ | 0        | $\sim1 = 0$      |
| $\&$     | 1        | $1 \& 1 = 1$     |
| $\hat{}$ | 2        | $1 | 0 = 1$      |
| $|$      | 2        | $1 \hat{} 1 = 0$ |

#### Boolean operators

| Operator | Priority | Example                   |
| -------- | -------- | ------------------------- |
| $!$      | 0        | $!true = false$           |
| $\&\&$   | 1        | $true \&\& false = false$ |
| $||$     | 2        | $false || true = true$    |

#### Comparison operators

| Operator | Priority | Example                                 |
| -------- | -------- | --------------------------------------- |
| $<$      | 0        | $2 < 4 = true$                          |
| $<=$     | 0        | $3 <= 3 = true$                         |
| $==$     | 0        | $5 == 5 = true$                         |
| $>$      | 0        | $4 > 2 = true$                          |
| $>=$     | 0        | $3 >= 3 = true$                         |
| $is$     | 0        | $null \text{ } is \text{ } null = true$ |

#### Other operators

##### Assignment operator

The assignment operator `=` assigns the value to its right in the variable to its left. It is **right-associative**.

```c#
int i = 4;
int j = i = 2;
// i == j == 2
```

##### Compound assignment operator

```c#
int a = 2;
a += 3; // equivalent to a = a + 3
// a == 5
```

The compound assignment operator works with any binary operator.

##### Member access operator

The member access operator `.` is used to access members of object instances, or static members of types.

```c#
string s = "42";
int n = int.Parse(s); // static method access
s = n.ToString(); // instance method access
```

##### Prefix and postfix increment and decrement operators

The prefix `++` operator increments a variable by 1, and returns the **new** value.

```c#
int a = 3;
int b = ++a;
// a == b == 4
```

The postfix `++` operator increments a variable by 1, and returns the **old** value.

```c#
int a = 3;
int b = a++;
// a == 4, b == 3
```

The `--` operator functions in an analogous way.

##### The new operator

The `new` operator must be used to call a type's constructor or to declare an array.

```c#
int[] array = new int[10]; // declare fixed-size array of 10 elements
List<int> list = new List<int>();
// List<int> list = List<int>(); // invalid, the constructor was not called using the new operator
```

### Control flow

*Note: Multiple statements can be grouped using the 'block statement' `{ <statements> }`.*

#### The return statement

```
return [<expression>];
```

The `return` statement stops the execution of the current method, and returns the value of `<expression>` back to the caller, if the return type of the method is not `void`.

#### The if statement

The `if` statement checks whether the `<boolean_expression>` equals to `true`, and executes `<statement1>`, otherwise it executes `<statement2>`, if the `else` branch exists.

```
if (<boolean_expression>)
	<statement1>;
[else
	<statement2>;]
```

```c#
if (1 > 2)
    System.Console.WriteLine("1 is greater than 2.");
else
    System.Console.WriteLine("1 is not greater than 2.");
```

#### The while statement

The `while` statement works in two steps:

1. Evaluate `<boolean_expression>`
2. If the expression evaluated at step 1 equals `true`, execute the `<statement>` and return to step 1

```
while (<boolean_expression>)
	<statement>;
```

```c#
int i = 0;
while (i < 5)
    System.Console.WriteLine("The value of i is: {0}", i++);
```

#### The do statement

The `do` statement works similarly to the `while` statement. The difference is that is evaluates the `<boolean_expression>` after executing the `<statements>`. Thus, the `<statements>` are executed **at least once**.

```
do {
	<statements>;
} while (<boolean_expression>);
```

```c#
int i = 42;
do {
    System.Console.WriteLine("This will be executed once.");
} while (i == 0);
```

#### The for statement

The `for` statement works in three steps:

1. Execute the `<loop_initializer>` if it is present; it can be a variable declaration, or a variable initialization.
2. Evaluate `<boolean_expression>`; if it is absent, it is assumed to be `true`.
3. If the expression evaluated at step 2 equals `true`, execute the `<statement>`, and then the `<post_loop_statement>`, then return to **step 2**.

```
for ([<loop_initializer>]; [<boolean_expression>]; [<post_loop_statement>])
	<statement>;
```

```c#
for (int i = 0; i < 5; ++i)
    System.Console.WriteLine("The value of i is: {0}", i);

int j = 0;
for (; j < 5; )
	System.Console.WriteLine("The value of j is: {0}", j++);
```

#### The break statement

The `break` statement forces the program to leave the body of the inner-most looping construct.

```c#
for (int i = 0; i < 5; ++i)
    for (int j = 0; j < 42; ++j)
    {
        System.Console.WriteLine("i = {0}, j = {1}", i, j);
        if (j == 1)
            break;
    }
```

#### The continue statement

The `continue` statement forces the program to jump to the **next iteration** of the inner-most looping construct.

```c#
for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
    {
        if (i == j)
            continue;
        System.Console.WriteLine("i = {0}, j = {1}", i, j);
    }
```

#### The switch statement

The `switch` statement compares the `<match_expression>` to all of the `<expression>`s declared in the `case` labels. If a match is found, the program jumps at the respective `case` label, otherwise it jumps at the `default` label, if it exists.

```
switch (<match_expression>)
{
	[case <expression>: [<statements>; [break;]]]
	[default: [<statements>; [break;]]]
}
```

```c#
int i = 2;
switch (i)
{
    case 2:
        System.Console.WriteLine("i is equal to 2.");
        // execution 'falls-through', because there is no break statement
    case 1:
        System.Console.WriteLine("i is equal to 1.");
        break;
    default:
        System.Console.WriteLine("i is equal to something else");
}
```

### Type declaration

```
[<access_modifier>] [static|abstract|sealed] class|interface|struct|enum <type_name>
	[: <type1>[, <type2>...]]
{
}
```

`<type_name>` represents the name of the new type to be declared. As C# does not support multiple inheritance, the list of types in the inheritance clause `[: <type1>[, <type2>...]]` must contain **at most** one name of class that is not marked as `sealed`, and it must be the first type in the list. If the declared type does not explicitly inherit from a class, then it inherits implicitly from `object`; `object` is the ultimate base class for all `.NET` types.

```c#
public class MyFirstClass
{
}
```

*<u>Code style</u>: Declare types using the UpperCamelCase naming convention. Each type should be declared in its own file that matches its name.*

### Method declaration

```
[<access_modifier>] [static|abstract|virtual] <return_type> <method_name>([<parameter_list>])
{
}
```

The following code declares a method named `TheAnswer` that returns an `int`, and takes no parameters.

```c#
public int TheAnswer()
{
    return 42;
}
```

*<u>Code style</u>: Declare methods using the UpperCamelCase naming convention.*

#### Optional parameters

The following code declares the same method, that takes an **optional** parameter of type `string`. If the caller doesn't specify a value when calling the method, the `question` parameter will have its default value.

```c#
public int TheAnswer(string question = "What is the answer to life, the Universe, and everything?")
{
    return 42;
}
```

*<u>Code style</u>: Declare parameters using the lowerCamelCase naming convention.*

#### Method overloading

In C#, methods are identified by their name, the number of parameters, and their order and type. That is, two methods are considered to be identical if they have the same name, take the same number of parameters, and the parameters' respective types match. *Note: The return type of the method does not identify a method.*

The following methods have the same **signature**; the compiler will emit an error, because there cannot be two methods with the same **signature**.

```c#
public void Method(int i, float j) { }
public string Method(int j, float i) { }
```

A method **overload** is a method with the same name, but with a different number of parameters, or with different types of parameters.

```c#
public int Add(int x, int y) { return x + y; }
public float Add(float x, float y) => x + y;
```

Both methods are called **overloads** of the `Add` method. The second declaration uses an **expression body** to represent the body of the method. Expression bodies can instead of a single statement; an expression body is essentially equivalent to `return <expression>`.

The correct overload that gets called is resolved at **compile-time**.

### Variable and constant declaration

Variables can be declared anywhere inside a method body using the following syntax:

```
<type> <identifier>[=<expression>];
```

`<type>` must be a valid type in the current scope. If the variable is initialized when it is declared, the keyword `var` can also be used; in this case, the variable's type is the same as the type of the `<expression>` that was used to initialize it.

```c#
var i = 42; // i is of type int
string s;
```

*<u>Code style</u>: Declare variables using the lowerCamelCase naming convention. Avoid the use of the `var` keyword.*

Constants are declared using a similar syntax:

```
[<access_modifier>] const <type> <identifier>=<expression>;
```

`<type>` cannot be substituted by the keyword `var` when declaring constants. `<expression>` must be able to be evaluated at compile-time. Constants can be declared directly on classes, but they can also be declared inside methods; in that case, they are called **local constants**.

*<u>Code style</u>: Declare constants using the UpperCamelCase or the ALL_UPPER naming convention.*

### Field and property declaration

Fields and properties must be declared on a class, at the same level as methods. The fundamental difference between fields and properties is that when accessing properties, there is a method call that retrieves or sets the value of the respective property. This means the programmer can implement custom logic for **property accessors**.

```c#
public class MySecondClass
{
    public const double PI = 3.14159;
    
    public int field;
    public string AutoProperty { get; set; }
    
    string autoPropertyBackingField;
    // The AutoProperty property above is implemented implicitly
    // by the compiler the same way as the following property
    public string ExplicitAutoProperty
    {
        get => autoPropertyBackingField;
        set => autoPropertyBackingField = value;
    }
    
    float propertyBackingField;
    public float Property
    {
        get { return propertyBackingField / 2; }
        set { propertyBackingField = value * 2; }
    }
}
```

The `get` and `set` keywords are **property accessors**, and are called **implicitly** when you access the respective property on an object instance for reading, and respectively writing the value of the property. Inside the `set` accessor, the special keyword `value` is available, and it represents the value to the right of the assignment operator.

*<u>Code style</u>: Use properties instead of public fields. Declare fields using the lowerCamelCase naming convention; declare properties using the UpperCamelCase naming convention. Use auto-properties when possible. Use expression bodies for property accessors when possible.*

### Namespaces

In C#, a namespace represents a grouping of classes that are related with each other. A namespace is declared using the `namespace` keyword. The following two namespace declarations are equivalent.

```c#
namespace Introduction
{
    namespace Inner
    {
    }
}
```

```c#
namespace Introduction
{
}
namespace Introduction.Inner
{
}
```

Every type declaration should be inside a namespace; even though a global namespace does exist. A type's **FQN** (fully-qualified-name) is represented by its namespace followed by a dot and the type's name. For example `Animals.Dog` represents the `Dog` class that resides in the `Animals` namespace. The `using` keyword is used to import namespaces in the current file, and allows the programmer to reference types without using their FQN. Namespaces are **open-ended**, you can declare the same namespace across multiple files, and all the declarations inside the namespace are available to any code importing the namespace.

```c#
using Introduction;
// the following is invalid, there is no Inner namespace
// using Inner;
using Introduction.Inner;
```

*<u>Code style</u>: Declare namespaces using the UpperCamelCase naming convention. Declare inner namespaces using the dot notation.*

### The 'Hello World!' program

The following is a simple program that prints 'Hello World!' to the screen and presents the basic structure of a C# program.

```c#
using System;

namespace Introduction
{
    class HelloWorld
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
```

The `Main` method is the **entry point** of every C# program and it will **always** be the first method executed. The `args` parameter is optional; it contains the arguments passed to the command line when invoking the program. There must be only one main method and it must be declared as `static`.

`Console` is a class from the `System` namespace, that exposes methods for working with the terminal, such as reading or writing.

### Object instances

In the OOP paradigm, every variable refers to an object **instance**.

#### Constructors

A constructor is a special method; it does not have a return type, and its name matches the declaring class's name. A constructor is an essential method of a class, as it allows the programmer to create instances of the respective class. A constructor must initialize the state of the class, such as setting values to its fields and properties. If a constructor is not explicitly declared, the compiler implicitly generates a public parameterless one. A constructor can only be called using the `new` operator.

#### Destructors

A destructor (also called a finalizer) is the counterpart method to the constructor; it does not have a return type, and its name matches the declaring class's name, with a `~` prefix. A class can only have one destructor, and it must not have parameters; it does not have an accessibility modifier, it cannot be called directly. The destructor is called right before the object instance is garbage collected. It must release resources, such as file handles.

#### Garbage collection

The C# language features a [garbage collector](https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/). Its purpose is to free up memory that is no longer used by the program. The programmer cannot explictly free resources used by an object; instead object instances are marked for garbage collection when there are **no references** to them. The programmer has limited control over the garbage collector using methods inside the `GC` class.

Consider the following program:

```c#
using System;

namespace Introduction.Instances
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rect = new Rectangle();
            rect.Width = 5;
            rect.Height = 6;
            Console.WriteLine("The area of rect is {0}.", rect.CalculateArea());
            
            Rectangle copy = rect;
            copy.Width = 7;
            Console.WriteLine("The area of rect is {0}.", rect.CalculateArea());
            Console.WriteLine("The area of copy is {0}.", copy.CalculateArea());
        }
    }

    class Rectangle
    {
        public float Width;
        public float Height;

        public float CalculateArea()
        {
            return Width * Height;
        }
    }
}
```

First, the `Rectangle` class. The keyword `public` is called a access modifier (explained in the encapsulation chapter); it allows the programmer to access the fields and methods of the rectangle from the `Main` method.

Inside the `Main` method there is a variable declaration `rect` of type `Rectangle`, and it is assigned to a `new` **instance** of a `Rectangle`. The `new` **operator** allocates memory, and then calls the class's **constructor ** which creates the instance.

Access to an object instance's members is made using the member access operator. After the initialization of the `rect` variable, its `Width` and `Height` fields are assigned values, and its area is printed to the console.

Then, the `copy` variable is declared and initialized with the value of the `rect` variable. What actually happens, is that now both `rect` and `copy` point to the **same object instance**, because they hold the same **reference**. All the changes made to `copy` will be visible on `rect`.

### Value Types and Reference Types

In C#, classes are **reference types**, while structs are **value types**. If the keyword on the declaration of `Rectangle` is changed from `class` to `struct`, the code from the previous section will behave as expected, because value types are copied on assignment.

Reference types passed to methods will be passed 'by reference', all changes made to them inside the method will be visible upon leaving its context.

Value types passed to methods will be passed 'by value', a copy of the value will be made, and the parameter will have the value of the copy; the changes made inside the method are discarded upon leaving its context.

Value types can be passed by reference to methods by using one of the following keywords, when declaring the method **and** when calling the method.

1. `in` – the parameter will be passed by reference, but it **cannot** be modified inside the method
2. `ref` –  the parameter will be passed by reference, and it **can** be modified inside the method
3. `out` – the parameter will be passed by reference, and it **must** be assigned a value inside the method

The `ref` and `out` keyword are useful when you want to return multiple values from a method, but creating a custom type would be unwarranted.

### The static keyword

The `static` keyword can appear in any member declaration. A `static` member is part of the **enclosing type**, rather than being part of an **instance**. Static members behave for the most part as **global variables**. Static members can only be accessed through the type's name. Constants are implicitly static. Non-static members are called **instance members**.

```c#
using System;

namespace Introduction.Static
{
    class Program
    {
        static void Main(string[] args)
        {
            Example instance1 = new StaticExample();
            Example instance2 = new StaticExample();
            Console.WriteLine("How many Examples have been instantiated? {0}", Example.Count);
            // this property does not have a set accessor, so the following is invalid
            // StaticExample.Count = 0;
        }
    }

    class Example
    {
        static int count;
        public static int Count => count;

        public StaticExample()
        {
            ++count;
        }
    }
}
```

### The this keyword

`this` is always available when not in a static context. `this` holds a reference to the **current instance** of the type that it is used in. `this` cannot be assigned to, but it can be passed as an argument to a method call. It is mostly used when a method parameter hides a field or property, because they have the same name (the term is officially called **hiding**, but **shadowing** is also common).

```c#
using System;

namespace Introduction.This
{
    class Program
    {
        static void Main(string[] args)
        {
            Example instance = new ThisExample();
            Console.WriteLine("The value of number is: {0}", instance.GetNumber());
            instance.SetNumber(10);
            Console.WriteLine("The value of number is: {0}", instance.GetNumber());
            instance.SetNumberWithThis(10);
            Console.WriteLine("The value of number is: {0}", instance.GetNumber());
        }
    }

    class Example
    {
        float number;

        public float GetNumber()
        {
            return number;
        }

        public void SetNumber(float number)
        {
            // incorrect, assignment was made to the number parameter
            number = number;
        }

        public void SetNumberWithThis(float number)
        {
            this.number = number;
        }
    }
}
```

*<u>Code style</u>: Only use the `this` keyword when necessary.*

### Exceptions

Exceptions are a special type of objects in C#. An exception is an object that represents an error that happened at runtime, due to different reasons. For example, `System.DivideByZeroException` is thrown when the programmer tries to integers by zero. Note that floating point division allows for division by zero.

The base class for exceptions is `System.Exception`. A programmer can define custom exceptions by extending this class.

#### Exception handling

Exceptions are handled by the programmer by wrapping code in a `try` block. A try clause must have at least a `catch` or `finally` clause. When an exception occurs in a try block, the execution of the block is stopped there, and control is handed to a corresponding catch block.

The `catch` clause specifies what type of exceptions to catch; the catch block specifies code to be executed when such an exception is caught.

The `finally` block specifies code to be run regardless of whether an exception was caught.

If an exception is not caught by the running code, it **bubbles** up the call stack. If it is not caught when it reaches the top of the call stack, the program is terminated.

```c#
int b = 42;
int a = 0;
try
{
    a = b / 0;
    b = 12;
}
catch (System.DivideByZeroException exception)
{
    System.Console.WriteLine(exception);
}
finally
{
    b = 24;
}
System.Console.WriteLine("a = {0}, b = {1}", a, b);
```

#### Exception throwing

An exception can be throwing using the `throw` keyword. The object that it throws must be of type `System.Exception`.

```c#
void Parse(string input)
{
    if (input is null)
        throw new ArgumentException(nameof(input));
    // do something with input here
}
```

The `nameof` keyword transforms an identifier into a string. The transformation is made at **compile time**.

### Reading from the console

A program's input can come in the form of command-line arguments, that are present in the `string[] args` parameter of the `Main` method. If the programmer wishes to ask the user for input instead, they can use the `System.Console` class.

The usual method for reading input is `Console.ReadLine`.

```c#
using System;

namespace Introduction.Reading
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is your name? ");
            string name = Console.ReadLine();
            Console.WriteLine("Hello {0}!", name);
        }
    }
}
```

*Note: `Console.ReadLine` always returns a string, the programmer must parse and validate all input.*

Most `.NET` base types expose a public `Parse`, and a public `TryParse` method, for converting a string to the respective type. The `TryParse` method returns a `bool` value that indicates whether the parsing succeeded; the `Parse` method throws an `Exception` if the parsing fails.

```c#
using System;

namespace Introduction.Parsing
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 0;
            bool valid = false;
            while (valid == false)
            {
                Console.WriteLine("Please enter your age: ");
                string input = Console.ReadLine();
                valid = int.TryParse(input, out age);
            }
            Console.WriteLine("Your age is: {0}", age);
        }
    }
}
```
