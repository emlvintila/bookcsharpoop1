# Encapsulation

## Assemblies

An assembly is a program that is compiled and linked, producing either an executable or a library. Assemblies can be referenced by other assemblies in order to reuse code that is exposed by the referenced assembly.

## Access modifiers

All access modifiers can be applied to member declarations. Some access modifiers can also be applied to type declarations. The 4 main access modifiers, in order from the least restrictive to the most restrictive are:

1. public
2. internal
3. protected
4. private

### public

The `public` access modifier allows any code that holds an instance to an object of the respective type to access its members, and any code can access the `public static` members of the type.

The `public` access modifier is also valid on type declarations. A public type is visible to code running in **any assembly**.

### internal

The `internal` access modifier restricts access to instance members and static members to code running in the **current assembly**.

The `internal` access modifier is also valid on type declarations. An internal type is only visible to code running in the **current assembly**.

`internal` is the **implicit** access modifier for **type declarations**.

### protected

The `protected` access modifier restricts access to instance and static members of the **enclosing type**, and any **derived type**.

### private

The `private` access modifier restricts access to instance and static members **only** of the **enclosing type**.

`private `is the **implicit** access modifier for **member declarations**.

*<u>Code style</u>: Use explicit access modifiers.*

## readonly fields and properties

A `readonly` field can only be initialized inside a constructor. The same applies to auto-implemented properties, as they use fields behind the scenes. A readonly property must declare only a get accessor, and must not be marked with the `readonly` keyword.

```c#
namespace Encapsulation.Accessiblity
{
    public class Program
    {
        private static void Main(string[] args)
        {
            PublicClass instance = new PublicClass();
            instance.internalField = "Internal fields are accessible.";
            instance.publicField = 3.14159;
            // private members are not accessible
            // instance.privateField = instance.privateConst * 5;
            // readonly members cannot be assigned
            // instance.readonlyField = double.PositiveInfinity;
            // instance.ReadonlyProperty = double.MaxValue;
        }
    }

    public class PublicClass
    {
        private const int privateConst = 42;
        private int privateField;
        internal string internalField;
        public double publicField;

        public readonly double readonlyField;
        public double ReadonlyProperty { get; }

        public PublicClass()
        {
            readonlyField = 2.71828;
            ReadonlyProperty = 2.71828;
        }
    }
}
```

## The reference problem

```c#
using System.IO;

namespace Encapsulation.Reference
{
    public class ReferenceProblem
    {
        private static void Main(string[] args)
        {
            Logger logger = new Logger("log.txt");
            Worker worker = new Worker(logger);
            worker.DoWork();
            // the following statement is NOT valid, because the logger field is inaccessible
            // service.logger.CloseFile();
            // the following statement closes the Logger associated with the Worker instance above
            logger.CloseFile();
            // now the following call throws an Exception
            worker.DoWork();
        }
    }

    public class Worker
    {
        private Logger logger;

        public Worker(Logger logger) => this.logger = logger;

        public void DoWork()
        {
            // do something here...
            logger.Log("Did some work.");
        }
    }

    public class Logger
    {
        private StreamWriter file;

        public Logger(string filename) => file = new StreamWriter(filename);
        
        public void Log(string message) => file.WriteLine(message);

        public void CloseFile() => file.Close();
    }
}
```

The above example illustrates the problem that arises because object instances are passed to methods by reference; that is why the programmer must be careful with object instances.

A straightforward solution to circumventing the problem is to create the `Logger` instance inside the `Service` constructor. But, this creates an **unwanted dependency**, the `Service` class must know how to construct a `Logger` instance, and if the `Logger` constructor signature ever changes, then the `Service` class must also update the call, and possibly change its constructor signature as well.

## The dependency inversion principle

The [**dependency inversion principle**](https://en.wikipedia.org/wiki/Dependency_inversion_principle) is a specific form of decoupling software modules. When following this principle, the conventional dependency relationships established from high-level, policy-setting modules to low-level, dependency modules are reversed, thus rendering high-level modules independent of the low-level module implementation details. The principle states:

1. High-level modules should not depend on low-level modules. Both should depend on abstractions (e.g. interfaces).
2. Abstractions should not depend on details. Details (concrete implementations) should depend on abstractions.

The idea behind points 1 and 2 of this principle is that when designing the interaction between a high-level module and a low-level one, the interaction should be thought of as an abstract interaction between them. This not only has implications on the design of the high-level module, but also on the low-level one: the low-level one should be designed with the interaction in mind and it may be necessary to change its usage interface.

![Dependency Injection Principle](..\Images\2-DIP.png)

The figures 1 and 2 illustrate code with the same functionality, however in figure 2, the dependency has been inverted using an interface. The direction of dependency can be chosen to maximize code reuse, and eliminate cyclic dependencies.

The usual implementation of the dependency inversion principle is called **dependency injection**. It is an advanced technique that will not be discussed in this book.

