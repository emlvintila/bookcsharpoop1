# Exercises

## SOLID

Throughout the first 4 chapters, you have learned about five principles. Those principles are called the **[SOLID](https://en.wikipedia.org/wiki/SOLID)** design principles. The following is a short description of the principles, in order of their appearance in the SOLID acronym.

1. Single responsibility principle
   A class should only have a single responsibility, that is, only changes to one part of the software's specification should be able to affect the specification of the class.
2. Open–closed principle
   "Software entities ... should be open for extension, but closed for modification."
3. Liskov substitution principle
   "Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program."
4. Interface segregation principle
   "Many client-specific interfaces are better than one general-purpose interface."
5. Dependency inversion principle
   One should "depend upon abstractions, [not] concretions."

## Test driven

The following exercises should help you get accustomed to C#, and to the SOLID design principles. Each exercise will have a minimum amount of source code provided, along with test code. The test code is written using the [NUnit framework](https://nunit.org/). You are advised to read the tests before you start writing the implementation, as they will give you valuable information about what your code is expected to do. You **must not** modify the tests, as they assure you that your code works correctly when all of them pass.

Visual Studio is able to run these tests using its test runner. You need to install the `NUnit3`, `NUnit3TestAdapter`, and `Microsoft.NET.Test.Sdk` packages using the NuGet package manager in order to run them.

A reference implementation is also provided. You are strongly advised to read it **only** after you solved the problem yourself, or if you got stuck.

## 1. Shapes

Implement the following shape hierarchy. This exercise demonstrates the use of interfaces and inheritance.

![Shape hierarchy](../Images/1560159680720.png)

### Starting code

```c#
namespace Exercises.Shapes
{
    public interface IShape
    {
        float GetArea();

        float GetPerimeter();
    }

    public class Rectangle : IShape
    {
        public float Diagonal { get; }

        public Rectangle(float width, float height) { }
    }

    public class Square : Rectangle
    {
        public Square(float length) : base(length, length) { }
    }

    public class Circle : IShape
    {
        public Circle(float radius) { }
    }
}
```

### Test code

```c#
using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;

namespace Exercises.Shapes
{
    [TestFixture]
    [DefaultFloatingPointTolerance(1e-3)]
    public class ShapeTests
    {
        [Test]
        public void TestArea()
        {
            IShape rectangle = new Rectangle(3, 4);
            Assert.AreEqual(12, rectangle.GetArea());

            IShape square = new Square(5);
            Assert.AreEqual(25, square.GetArea());

            IShape circle = new Circle(6);
            Assert.AreEqual(6 * Math.PI * Math.PI, circle.GetArea());
        }

        [Test]
        public void TestPerimeter()
        {
            IShape rectangle = new Rectangle(3, 4);
            Assert.AreEqual(14, rectangle.GetPerimeter());

            IShape square = new Square(5);
            Assert.AreEqual(20, square.GetPerimeter());

            IShape circle = new Circle(6);
            Assert.AreEqual(12 * Math.PI, circle.GetPerimeter());
        }

        [Test]
        public void TestDiagonal()
        {
            Rectangle rectangle = new Rectangle(3, 4);
            Assert.AreEqual(5, rectangle.Diagonal);

            Rectangle square = new Square(5);
            Assert.AreEqual(5 * Math.Sqrt(2), square.Diagonal);
        }

        [Test]
        public void TestNegativeLengthThrows([Random(float.MinValue, -1, 5)] float negative)
        {
            float positive = -negative;

            Assert.Throws<ArgumentOutOfRangeException>(() => new Rectangle(negative, negative));
            Assert.Throws<ArgumentOutOfRangeException>(() => new Rectangle(negative, positive));
            Assert.Throws<ArgumentOutOfRangeException>(() => new Rectangle(positive, negative));

            Assert.Throws<ArgumentOutOfRangeException>(() => new Square(negative));

            Assert.Throws<ArgumentOutOfRangeException>(() => new Circle(negative));
        }

        [Test]
        public void TestNaNThrows()
        {
            Assert.Throws<ArgumentException>(() => new Rectangle(float.NaN, 1));
            Assert.Throws<ArgumentException>(() => new Rectangle(1, float.NaN));
            Assert.Throws<ArgumentException>(() => new Rectangle(float.NaN, float.NaN));

            Assert.Throws<ArgumentException>(() => new Square(float.NaN));

            Assert.Throws<ArgumentException>(() => new Circle(float.NaN));
        }
    }
}
```

### Reference implementation

```c#
using System;

namespace Exercises.Shapes
{
    public interface IShape
    {
        float GetArea();

        float GetPerimeter();
    }

    public class Rectangle : IShape
    {
        private readonly float width;
        private readonly float height;

        public float Diagonal { get; }

        public Rectangle(float width, float height)
        {
            if (width <= 0 || height <= 0)
                throw new ArgumentOutOfRangeException();
            if (float.IsNaN(width) || float.IsNaN(height))
                throw new ArgumentException();

            this.width = width;
            this.height = height;
            Diagonal = (float) Math.Sqrt(width * width + height * height);
        }

        public float GetArea() => width * height;

        public float GetPerimeter() => 2 * (width + height);
    }

    public class Square : Rectangle
    {
        public Square(float length) : base(length, length) { }
    }

    public class Circle : IShape
    {
        private readonly float radius;

        public Circle(float radius)
        {
            if (radius <= 0)
                throw new ArgumentOutOfRangeException();
            if (float.IsNaN(radius))
                throw new ArgumentException();

            this.radius = radius;
        }

        public float GetArea() => (float) (radius * Math.PI * Math.PI);

        public float GetPerimeter() => (float) (2 * radius * Math.PI);
    }
}
```

## 2. Complex numbers

Implement a `Complex` class with all the relevant operations. This exercise demonstrates **operator overloading**, and **conversion operators**.

The `.NET` framework already supplies a type for working with complex numbers – `System.Numerics.Complex`.

### Operator overloading

Most operators can be overloaded when declaring a class. In this exercise you will overload the 4 arithmetic operators. Overloaded operators **must be public and static**.

```
public static <return_type> operator <symbol>([<parameters>])
```

### Conversion operators

A conversion operator is an operator that takes the declared type and returns another type, or the other way around. For example, a real number can be converted into a complex number, by specifying that the imaginary part is zero.

Conversion operators can be `implicit` or `explicit`. Explicit operators require the programmer to do an explicit cast.

```
public static implicit|explicit operator <to_type>(<from_type> <parameter_name>)
```

### The  IEquatable\<T\> interface

The `IEquatable<T>` is a generic interface (explained in the generics chapter) that should be implemented by types that should be checked for value equality. It exposes a single method `bool Equals(T)` that should return whether two instances hold the same value.

Whenever a type implements the `IEquatable<T>` interface, it should also override the `object.Equals` method to use the same implementation.

### Starting code

```c#
using System;

namespace Exercises.ComplexNumbers
{
    public struct Complex : IEquatable<Complex>
    {
        public Complex(float real, float imaginary) { }

        public Complex Conjugate() { }

        public float AbsoluteValue() { }
        
        public static implicit operator Complex(float real) => new Complex(real, 0);

        public static Complex operator +(Complex left, Complex right) { }

        public static Complex operator -(Complex left, Complex right) { }
        
        public static Complex operator -(Complex complex) { }

        public static Complex operator *(Complex left, Complex right) { }

        public static Complex operator /(Complex left, Complex right) { }
        
        public bool Equals(Complex other)
        {
            const float DELTA = 1e-3f;
            return Math.Abs(Real - other.Real) < DELTA &&
            	Math.Abs(Imaginary - other.Imaginary) < DELTA;
        }

        public override bool Equals(object obj) => obj is Complex other && Equals(other);
    }
}
```

### Test code

```c#
using NUnit.Framework;

namespace Exercises.ComplexNumbers
{
    [TestFixture]
    [DefaultFloatingPointTolerance(1e-3f)]
    public class ComplexTests
    {
        [Test]
        public void TestAddition()
        {
            Complex c1 = new Complex(4, 2);
            Complex c2 = new Complex(8, 6);

            Assert.AreEqual(new Complex(12, 8), c1 + c2);
            Assert.AreEqual(new Complex(5, 2), 1 + c1);
        }

        [Test]
        public void TestSubtraction()
        {
            Complex c1 = new Complex(3 , 7);
            Complex c2 = new Complex(15, 3);
            Complex expected = new Complex(-12, 4);
            
            Assert.AreEqual(expected, c1 - c2);
            Assert.AreEqual(expected, -(c2 - c1));
            Assert.AreEqual(c1, -(-c1));
        }

        [Test]
        public void TestMultiplication()
        {
            Complex c1 = new Complex(2, 3);
            Complex c2 = new Complex(4, 5);
            Complex expected = new Complex(-7, 22);

            Assert.AreEqual(expected, c1 * c2);
            Assert.AreEqual(new Complex(4, 6), 2 * c1);
        }
        
        [Test]
        public void TestDivision()
        {
            Complex c1 = new Complex(3, 5);
            Complex c2 = new Complex(6, 4);
            Complex expected = new Complex(38, 18) / 52;
            
            Assert.AreEqual(expected, c1 / c2);
            Assert.AreEqual(1 / expected, c2 / c1);
        }

        [Test]
        public void TestFloatConversion()
        {
            Complex complex = 42;
            Complex expected = new Complex(42, 0);
            
            Assert.AreEqual(expected, complex);
        }

        [Test]
        public void TestConjugate()
        {
            Complex conjugate = new Complex(5, 4).Conjugate();
            Complex expected = new Complex(5, -4);
            
            Assert.AreEqual(expected, conjugate);
        }

        [Test]
        public void TestAbsoluteValue()
        {
            Complex complex = new Complex(3, 4);
            Complex negative = new Complex(-3, -4);
            Complex altern = new Complex(-3, 4);
            
            Assert.AreEqual(5, complex.AbsoluteValue());
            Assert.AreEqual(5, negative.AbsoluteValue());
            Assert.AreEqual(5, altern.AbsoluteValue());
        }
    }
}
```

### Reference implementation

```c#
using System;

namespace Exercises.ComplexNumbers
{
    public struct Complex : IEquatable<Complex>
    {
        public float Real { get; }
        public float Imaginary { get; }

        public Complex(float real, float imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }

        public Complex Conjugate() => new Complex(Real, -Imaginary);

        public float AbsoluteValue() => (float) Math.Sqrt(Real * Real + Imaginary * Imaginary);

        public static implicit operator Complex(float real) => new Complex(real, 0);

        public static Complex operator +(Complex left, Complex right) =>
            new Complex(left.Real + right.Real, left.Imaginary + right.Imaginary);

        public static Complex operator -(Complex left, Complex right) =>
            new Complex(left.Real - right.Real, left.Imaginary - right.Imaginary);

        public static Complex operator -(Complex complex) => new Complex(-complex.Real, -complex.Imaginary);

        public static Complex operator *(Complex left, Complex right) =>
            new Complex(left.Real * right.Real - left.Imaginary * right.Imaginary,
                left.Real * right.Imaginary + left.Imaginary * right.Real);

        public static Complex operator /(Complex left, Complex right)
        {
            Complex numerator = left * right.Conjugate();
            float abs = right.AbsoluteValue();
            float abssqr = abs * abs;
            return new Complex(numerator.Real / abssqr, numerator.Imaginary / abssqr);
        }

        public bool Equals(Complex other)
        {
            const float DELTA = 1e-3f;
            return Math.Abs(Real - other.Real) < DELTA &&
            	Math.Abs(Imaginary - other.Imaginary) < DELTA;
        }

        public override bool Equals(object obj) => obj is Complex other && Equals(other);

        public static bool operator ==(Complex left, Complex right) => left.Equals(right);

        public static bool operator !=(Complex left, Complex right) => !left.Equals(right);

        public override string ToString() => $"{Real:F2} + {Imaginary:F2} i";
    }
}
```

## 3. Divisors

Determine all the positive divisors (including 1 and itself) of a given integer. This exercise demonstrates the use of `IEnumerable<T>` interface, and iterators. 

Note that your implementation isn't required to return the divisors in any particular order.

### The IEnumerable\<T\> interface

The `IEnumerable<T>` interface is implemented by all of the collections in the `.NET` framework. The `System.Linq` namespace defines **extension methods** for working with collections, such as `Sum`. Any object that implements the `IEnumerable` interface can be iterated over using the `foreach` construct.

#### foreach

```
foreach (<type> <identifier> in <collection>)
	<statement>;
```

Note that you cannot reassign to the iteration variable in order to modify the collection.

```c#
IEnumerable<int> array = new int[] {1, 2, 3};
foreach (int item in array)
{
    // item = item * 2; // this is invalid
    Console.WriteLine(item);
}
System.Console.WriteLine(array.Sum());
for (int i = 0; i < array.Length; ++i)
{
    array[i] = array[i] * 2; // this is valid
    i = array.Length; // this is also valid
    Console.WriteLine(array[i]);
}
Console.WriteLine(array.Sum());
```

#### yield

The `yield` keyword is used inside a method body that returns an `IEnumerable` to yield a value, by composing it with the return statement:  `yield return <expression>;`.

The `yield break` statement is used to specify the end of an iterator method.

An `IEnumerable` can represent a collection that was enumerated; or it can represent a query. For example:

```c#
IEnumerable<int> Range(int low, int high)
{
    for (int i = low; i < high; ++i)
        yield return i;
    Console.WriteLine("Done enumerating from {0} to {1}.", low, high);
}
```

When the `yield return` statement is encountered, control flow is given back to the caller. Only after the `IEnumerable` is **enumerated**, by a `foreach` statement, or by casting it to a collection type, such as `List` using the `ToList` extension method, then the entire body of the method will be executed; this is called **lazy evaluation**.

```c#
IEnumerable<int> range = Range(0, 5);
// Nothing was displayed on the screen
List<int> list = range.ToList();
// The message was displayed on the screen now
foreach (int number in range)
    Console.WriteLine(number);
// The numbers were displayed on the screen, and then the message
```

### Starting code

```c#
using System.Collections.Generic;

namespace Exercises.Divisors
{
    public static class Divisors
    {
        public static IEnumerable<int> Of(int number) { }
    }
}
```

### Test code

```c#
using System.Collections.Generic;
using System.Collections.Immutable;
using NUnit.Framework;

namespace Exercises.Divisors
{
    [TestFixture]
    public class DivisorsTests
    {
        [Test]
        public void TestDivisors()
        {
            IEnumerable<int> expected = new[] {1, 2, 3, 6};
            IEnumerable<int> actual = Divisors.Of(6).ToImmutableSortedSet();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestNegative()
        {
            IEnumerable<int> expected = new[] {1, 2, 4};
            IEnumerable<int> negative = Divisors.Of(-4).ToImmutableSortedSet();
            
            Assert.AreEqual(expected, negative);
        }

        [Test]
        public void TestPrime()
        {
            IEnumerable<int> expected = new[] {1, 7};
            IEnumerable<int> primes = Divisors.Of(7).ToImmutableSortedSet();
            
            Assert.AreEqual(expected, primes);
        }

        [Test]
        public void TestOne()
        {
            IEnumerable<int> expected = new[] {1};
            IEnumerable<int> actual = Divisors.Of(1).ToImmutableSortedSet();
            
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestZero()
        {
            IEnumerable<int> actual = Divisors.Of(0);
            
            Assert.IsEmpty(actual);
        }
    }
}
```

### Reference implementation

#### Implementation 1

This implementation does not make use of lazy evaluation.

```c#
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exercises.Divisors
{
    public static class Divisors
    {
        public static IEnumerable<int> Of(int number)
        {
            if (number == 0)
                return new int[0];

            number = Math.Abs(number);
            List<int> divisors = new List<int>();
            for (int i = 1, high = number / 2; i <= high; ++i)
                if (number % i == 0)
                    divisors.Add(i);
            divisors.Add(number);

            return divisors;
        }
    }
}
```

#### Implementation 2

```c#
using System;
using System.Collections.Generic;

namespace Exercises.Divisors
{
    public static class Divisors
    {
        public static IEnumerable<int> Of(int number)
        {
            if (number == 0)
                yield break;

            number = Math.Abs(number);
            for (int i = 1, high = number / 2; i <= high; ++i)
                if (number % i == 0)
                    yield return i;
            yield return number;
        }
    }
}
```

## 4. Perfect numbers

Determine if a number is perfect, abundant, or deficient based on Nicomachus' (60 - 120 CE) classification scheme for natural numbers.

The Greek mathematician [Nicomachus](https://en.wikipedia.org/wiki/Nicomachus) devised a classification scheme for natural numbers, identifying each as belonging uniquely to the categories of **perfect**, **abundant**, or **deficient** based on their [aliquot sum](https://en.wikipedia.org/wiki/Aliquot_sum). The aliquot sum is defined as the sum of the factors of a number not including the number itself. For example, the aliquot sum of 15 is (1 + 3 + 5) = 9

- Perfect: aliquot sum = number
  - 6 is a perfect number because (1 + 2 + 3) = 6
  - 28 is a perfect number because (1 + 2 + 4 + 7 + 14) = 28
- Abundant: aliquot sum > number
  - 12 is an abundant number because (1 + 2 + 3 + 4 + 6) = 16
  - 24 is an abundant number because (1 + 2 + 3 + 4 + 6 + 8 + 12) = 36
- Deficient: aliquot sum < number
  - 8 is a deficient number because (1 + 2 + 4) = 7
  - Prime numbers are deficient

In order to represent the categories of the numbers, you will use an `enum`. If the number is not positive, throw an `ArgumentException`.

### The enum type

An `enum` is a type that derives from an integer type; it derives from `int` implicitly. An enum only declares a list of 'values' that the enumeration type can take, with explicit values optionally.

```
enum <enum_name> [: <integer_type>]
{
	[<identifier> [=<constant_expression>], [...]]
}
```

### Starting code

```c#
using System;
using System.Collections.Generic;
using System.Linq;
using Exercises.Divisors;

namespace Exercies.PerfectNumbers
{
    public static class PerfectNumbers
    {
        public static NumberType Type(int number) { }
    }
    
    public enum NumberType
    {
        Deficient,
        Perfect,
        Abundant
    }
}
```

### Test code

```c#
using System;
using NUnit.Framework;

namespace Exercies.PerfectNumbers
{
    [TestFixture]
    public class PerfectNumbersTests
    {
        [Test]
        public void TestPerfect()
        {
            Assert.AreEqual(NumberType.Perfect, PerfectNumbers.Type(6));
            Assert.AreEqual(NumberType.Perfect, PerfectNumbers.Type(28));
        }

        [Test]
        public void TestDeficient()
        {
            Assert.AreEqual(NumberType.Deficient, PerfectNumbers.Type(7));
            Assert.AreEqual(NumberType.Deficient, PerfectNumbers.Type(13));
            Assert.AreEqual(NumberType.Deficient, PerfectNumbers.Type(15));
            Assert.AreEqual(NumberType.Deficient, PerfectNumbers.Type(17));
        }

        [Test]
        public void TestAbundant()
        {
            Assert.AreEqual(NumberType.Abundant, PerfectNumbers.Type(12));
            Assert.AreEqual(NumberType.Abundant, PerfectNumbers.Type(24));
        }
        
        [Test]
        public void TestOne()
        {
            Assert.AreEqual(NumberType.Deficient, PerfectNumbers.Type(1));       
        }

        [Test]
        public void TestNonPositive()
        {
            Assert.Throws<ArgumentException>(() => PerfectNumbers.Type(0));
            Assert.Throws<ArgumentException>(() => PerfectNumbers.Type(-1));
        }
    }
}
```

### Reference implementation

This implementation uses the `Exercises.Divisors.Divisors.Of` method from exercise 3.

```c#
using System;
using System.Collections.Generic;
using System.Linq;
using Exercises.Divisors;

namespace Exercies.PerfectNumbers
{
    public static class PerfectNumbers
    {
        public static NumberType Type(int number)
        {
            if (number <= 0)
                throw new ArgumentException();
            IEnumerable<int> divisors = Divisors.Of(number).Where(divisor => divisor != number);
            int sum = divisors.Sum();
            if (sum > number)
                return NumberType.Abundant;
            if (sum == number)
                return NumberType.Perfect;
            return NumberType.Deficient;
        }
    }
    
    public enum NumberType
    {
        Deficient,
        Perfect,
        Abundant
    }
}
```

## 5. Levenshtein distance

The [**Levenshtein distance**](https://en.wikipedia.org/wiki/Levenshtein_distance) is a string metric for measuring the difference between two sequences. Informally, the Levenshtein distance between two words is the minimum number of single-character edits (insertions, deletions or substitutions) required to change one word into the other.

Mathematically, the Levenshtein distance between two strings $a, b$ of length $|a|, |b|$ is given by $lev_{a,b}(|a|, |b|)$, where:

$$
\begin{align*}
    lev_{a,b}(i, j) = \left\{
        \begin{array}{@{}ll@{\thinspace}l}
            max(i, j) & \text{if } min(i,j) = 0, \\
            min \left\{
                \begin{array}{@{}ll@{\thinspace}l}
                    lev_{a,b}(i-1, j) + 1 \\
                    lev_{a,b}(i, j-1) + 1 \\
                    lev_{a,b}(i-1, j-1) + 1_{a_i \ne b_i}
                \end{array}
            \right. & \text{otherwise.}
        \end{array}
    \right.
\end{align*}
$$
where $1_{a_i \ne b_j}$ is the indicator function equal to 0 when $a_i = b_j$ and equal to 1 otherwise, and $lev_{a,b}(i, j)$ is the distance between the first $i$ characters of $a$ and the first $j$ characters of $b$.

Your implementation should be case insensitive, and treat null strings as being empty. 

### Starting code

You may not modify the method signature, but you may define another method that will be called by this one.

```c#
namespace Exercises.EditDistance
{
    public static class EditDistance
    {
        public static int LevenshteinDistance(string left, string right) { }
    }
}
```

### Test code

```c#
using System.Data;
using NUnit.Framework;

namespace Exercises.EditDistance
{
    [TestFixture]
    public class LevenshteinDistanceTests
    {
        [Test]
        public void Test()
        {
            Assert.AreEqual(0, EditDistance.LevenshteinDistance("levenshtein", "levenshtein"));
            Assert.AreEqual(3, EditDistance.LevenshteinDistance("kitten", "sitting"));
            Assert.AreEqual(3, EditDistance.LevenshteinDistance("sitting", "kitten"));
        }
        
        [Test]
        public void TestCaseInsensitive()
        {
            const string lower = "lowercase";
            const string mixed = "LoWeRcAsE";
            
            Assert.AreEqual(0, EditDistance.LevenshteinDistance(lower, mixed));
        }

        [Test]
        public void TestSymbols()
        {
            const string alpha = "abcde";
            const string symbols = "!@#$%";
            const string mixed = "ab#de";
            
            Assert.AreEqual(5, EditDistance.LevenshteinDistance(alpha, symbols));
            Assert.AreEqual(1, EditDistance.LevenshteinDistance(alpha, mixed));
            Assert.AreEqual(4, EditDistance.LevenshteinDistance(symbols, mixed));
        }
        
        [Test]
        public void TestEmptyString()
        {
            const string str = "NotEmpty";
            
            Assert.AreEqual(str.Length, EditDistance.LevenshteinDistance(str, ""));
            Assert.AreEqual(str.Length, EditDistance.LevenshteinDistance("", str));
        }

        [Test]
        public void TestNullString()
        {
            const string str = "NotNull";
            
            Assert.AreEqual(str.Length, EditDistance.LevenshteinDistance(str, null));
            Assert.AreEqual(str.Length, EditDistance.LevenshteinDistance(null, str));
        }

        [Test]
        public void TestBothNullOrEmpty()
        {
            Assert.AreEqual(0, EditDistance.LevenshteinDistance(null, null));
            Assert.AreEqual(0, EditDistance.LevenshteinDistance(null, ""));
            Assert.AreEqual(0, EditDistance.LevenshteinDistance("", null));
            Assert.AreEqual(0, EditDistance.LevenshteinDistance("", ""));
        }
    }
}
```

### Reference implementation

#### Implementation 1

This is a naïve implementation, based off the mathematical definition of the function. It uses a **local function**, that is called recursively to solve the problem. The reason for not using a separate method is that it would need to be passed the strings as parameters, while the local function **captures** and works with their values.

```c#
using System.Linq;

namespace Exercises.EditDistance
{
    public static class EditDistance
    {
        public static int LevenshteinDistance(string left, string right)
        {
            left = left is null ? string.Empty : left.ToLowerInvariant();
            right = right is null ? string.Empty : right.ToLowerInvariant();

            int Inner(int leftLength, int rightLength)
            {
                if (leftLength == 0) return rightLength;
                if (rightLength == 0) return leftLength;

                int substitutionCost = 1;
                // note that this is different from left.Last() or left[left.Length - 1],
                // because the leftLength parameter is different for each call of the function
                if (left[leftLength - 1] == right[rightLength - 1])
                    substitutionCost = 0;

                return new[]
                {
                    Inner(leftLength - 1, rightLength) + 1,
                    Inner(leftLength, rightLength - 1) + 1,
                    Inner(leftLength - 1, rightLength - 1) + substitutionCost
                }.Min();
            }

            return Inner(left.Length, right.Length);
        }
    }
}
```

#### Implementation 2

This implementation is based off the [Wagner-Fischer algorithm](https://en.wikipedia.org/wiki/Wagner%E2%80%93Fischer_algorithm). It is more efficient because it doesn't recalculate the distances between substrings. The `Swap` function is a **local function** that swaps two arrays.

```c#
using System.Linq;

namespace Exercises.EditDistance
{
    public static class EditDistance
    {
        public static int LevenshteinDistance(string left, string right)
        {
            left = left is null ? string.Empty : left.ToLowerInvariant();
            right = right is null ? string.Empty : right.ToLowerInvariant();

            int[] vleft = Enumerable.Range(0, right.Length + 1).ToArray();
            int[] vright = new int[right.Length + 1];

            void Swap(ref int[] leftArray, ref int[] rightArray)
            {
                int[] temp = leftArray;
                leftArray = rightArray;
                rightArray = temp;
            }

            for (int i = 0; i < left.Length; ++i)
            {
                vright[0] = i + 1;
                for (int j = 0; j < right.Length; ++j)
                {
                    int deletionCost = vleft[j + 1] + 1;
                    int insertionCost = vright[j] + 1;
                    int substitutionCost = vleft[j];
                    if (left[i] != right[j])
                        ++substitutionCost;

                    vright[j + 1] = new[] {deletionCost, insertionCost, substitutionCost}.Min();
                }

                Swap(ref vleft, ref vright);
            }

            return vleft.Last();
        }
    }
}
```

