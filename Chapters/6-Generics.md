# Generic programming

## Basics

A **generic type** is a type that is unspecified in the **declaration** of a class, method, property, or field. A **generic class** is a class that is declared with at least one **generic type parameter**. Generic properties and fields can only appear inside a generic class. A generic method may be declared inside a non-generic class.

When instantiating a generic class, the programmer **must supply** an existing type to be substituted for the generic type parameter.

When calling a generic method, the types **may be inferred** from the method arguments, but, if the call is ambiguous, the programmer must supply types to the generic method call.

Generic type parameters are used between `<` and `>`.

The following method is declared with a generic type parameter named `T`. It takes a parameter of type `T`, and prints it to the screen.

```c#
void GenericMethod<T>(T obj) => System.Console.WriteLine(obj);
```

```c#
GenericMethod(4); // will print 4 on the screen
// the generic parameter is not needed as it can be inferred
GenericMethod<int>(2); // will print 2 on the screen
```

Generic types provide type-safety at **compile-time**.

```c#
// the following call is invalid, because 4.5 is of type double
// GenericMethod<int>(4.5);
```

### object-based stack

Imagine a stack collection with the usual implementation, push, pop etc., but the field used to hold the elements is an array of objects.

```c#
class Stack
{
    private object[] elements;
    public object Pop() { /* ... */ }
    public void Push(object element) { /* ... */ }
    public bool IsEmpty() { /* ... */ }
    /* ... */
}
```

The way this type of collection is used is:

```c#
Stack intStack = new Stack();
intStack.push(1);
intStack.push(2);
intStack.Push("something");
int sum = 0;
while (!intStack.IsEmpty())
{
    int top = (int) intStack.Pop();
    sum = sum + top;
}
```

The call to `Stack.Push` must convert the argument to `object`, which is inefficient; this is called a **boxing conversion**.

Note the cast to `int` at the `Stack.Pop` method call. This **unboxing conversion** from `object` to `int` is inefficient, and **NOT type safe**, because it cannot be caught by the compiler at compile-time, and it can fail at runtime. In this example it will fail when the string is popped from the stack.

### Generic stack

The same `Stack` class, but declared with one generic type parameter:

```c#
class Stack<T>
{
	private T[] elements;
	public T Pop() { /* ... */ }
	public void Push(T element) { /* ... */ }
	public bool IsEmpty() { /* ... */ }
	/* ... */
}
```

Note that any member can use the generic type `T` declared by the `Stack<T>` class; the `Pop` and `Push` methods aren't required to declare their own generic type parameters.

```c#
Stack<int> stack = new Stack<int>();
stack.Push(1);
stack.Push(2);
// the following is now invalid, the code does NOT compile
// stack.Push("something");
```

*<u>Code style</u>: Declare generic types using the UpperCamelCase naming convention, and use the 'T' prefix, if the name is longer than one character (example: T, U, TClass).*

## Type constraints

When declaring a generic type or method, the programmer can declare **generic type constraints** for the generic type parameters. If the constraint is not met by the supplied type, the error is caught at **compile-time**. 

```
... class|struct|interface <name><<generic>>
	[where <generic> : [<constraint>[, ...]]]
{
}

... <method_name><<generic>>([<parameter_list>])
	[where <generic> : [<constraint>[, ...]]]
{
}
```

- subtype constraint – The generic type must be a subtype of the specified type. The specified type can be another generic type from the same declaration.
- `new()` constraint – The generic type must expose a public parameterless constructor.
- `class` constraint – The generic type must be a reference type. It must come before any other constraints.
- `struct` constraint – The generic type must be a value type. It must come before any other constraints. It implies the `new()` constraint.

By specifying constraints for generic type parameters, the programmer is allowed to use the respective constraints inside the type or method declaration. For example, by specifying a subtype constraint, the compiler allows the programmer to use the members exposed by the constraining type, because any instance of the constrained type is guaranteed to be of the respective subtype.

The following declares a `Comparer` class with a generic type parameter `T`, that must implement the `IComparable<T>` interface. It means that an instance of `T` must be comparable with another instance of `T`.

```c#
class Comparer<T>
	where T : System.IComparable<T>
{
	// CompareTo is a method exposed by the IComparable<T> interface.
	public int Compare(T left, T right) => left.CompareTo(right);
}
```

```c#
Comparer<int> intComparer = new Comparer<int>();
System.Console.WriteLine(intComparer.Compare(4, 2));
// invalid, because bool does not implement the IComparable<bool> interface
// Comparer<bool> boolComparer = new Comparer<bool>();
```

Generic subtype constraint example:

```c#
class A { }
class B : A { }
class C { }

class X<TSupertype, TSubtype>
	where TSupertype: class
	where TSubtype : TSupertype
{
}
```

```c#
new X<A, A>(); // valid, A is a subtype of A
new X<A, B>(); // valid, B is a subtype of A
new X<B, B>(); // valid, B is a subtype of B

// new X<B, A>(); // invalid, A is a supertype of B, not a subtype
// new X<A, C>(); // invalid, C is not a subtype of A
```

## Inheritance

Generic types can inherit from non-generic types, or from other generic types. When inheriting from other generic types, the inheritor **may** provide type arguments for some of the generic type parameters.

```c#
class X<T1, T2> { }
class Y<T1, T2, T3> : X<T1, T2> { }
class Z<T1> : X<T1, int> { }
class W : X<int, int> { }
// the following declarations are invalid, T1 and T2 are not valid types in the respective context
// class V : X<T1, T2> { } 
// class U<T3> : X<T1, T2> { }
```

## Collections

The `.NET` framework provides generic collections inside the `System.Collections.Generic` namespace. Most of them implement the `IEnumerable<T>` interface and are serializable. The most common ones are:

- `List<T>` – A list of `T` that dynamically resizes when more items are added to it.
- `Queue<T>` – A queue of `T` that dynamically resizes and exposes basic queue methods.
- `Dictionary<TKey, TValue>` – A hash-based collection that implements `IDictionary<TKey, TValue>` and `IEnumerable<KeyValuePair<TKey, TValue>>`. Keys are placed in buckets based on the return value of the virtual `System.Object.GetHashCode` method. In order to efficiently use a type as a dictionary key, the hash function must be overridden and correctly implemented in order to produce as few collisions as possible. This type is not serializable.

