# Exercises

All of the following exercises require the understanding of events and delegates.

## 1. Observable Collection

Implement an `ObservableCollection<T>` class that holds an arbitrary number of elements. It must implement the `IEnumerable<T>` interface, and it must expose the `Add` and `Remove` methods. Additionally it must provide a way to subscribe to events related to adding and removing items from the collection.

### Starting code

```c#
using System.Collections.Generic;

namespace Exercises.ObservableCollection
{
    public class ObservableCollection<T> : IEnumerable<T>
    {
        public event CollectionChanged<T> CollectionChanged;
        
        public void Add(T item) { }
        public void Remove(T item) { }
    }
    
    public delegate void CollectionChanged<T>(ObservableCollection<T> collection, CollectionChangedEventArgs<T> args);
    
    public class CollectionChangedEventArgs<T>
    {
        public T Item { get; }
        public CollectionChangedEventType EventType { get; }

        public CollectionChangedEventArgs(T item, CollectionChangedEventType eventType)
        {
            Item = item;
            EventType = eventType;
        }
    }

    public enum CollectionChangedEventType
    {
        Add,
        Remove
    }
}
```

### Test code

```c#
using System.Collections.Generic;
using System.Linq;
using Exercises.ObservableCollection;
using NUnit.Framework;

namespace Exercises
{
    [TestFixture]
    public class ObservableCollectionTests
    {
        [Test]
        public void TestEvent()
        {
            ObservableCollection<int> collection = new ObservableCollection<int>();
            int addCount = 0;
            int addSum = 0;
            int removeCount = 0;
            int removeSum = 0;
            collection.CollectionChanged += (_, args) =>
            {
                if (args.EventType == CollectionChangedEventType.Add)
                {
                    ++addCount;
                    addSum += args.Item;
                }
                else
                {
                    ++removeCount;
                    removeSum += args.Item;
                }
            };
            collection.Add(1);
            Assert.AreEqual(1, addCount);
            Assert.AreEqual(1, addSum);
            Assert.AreEqual(0, removeCount);
            Assert.AreEqual(0, removeSum);
            collection.Add(41);
            Assert.AreEqual(2, addCount);
            Assert.AreEqual(42, addSum);
            Assert.AreEqual(0, removeCount);
            Assert.AreEqual(0, removeSum);
            collection.Remove(24);
            Assert.AreEqual(2, addCount);
            Assert.AreEqual(42, addSum);
            Assert.AreEqual(0, removeCount);
            Assert.AreEqual(0, removeSum);
            collection.Remove(41);
            Assert.AreEqual(2, addCount);
            Assert.AreEqual(42, addSum);
            Assert.AreEqual(1, removeCount);
            Assert.AreEqual(41, removeSum);
            collection.Remove(1);
            Assert.AreEqual(2, addCount);
            Assert.AreEqual(42, addSum);
            Assert.AreEqual(2, removeCount);
            Assert.AreEqual(42, removeSum);
        }

        [Test]
        public void TestIEnumerable()
        {
            IEnumerable<int> ints = new[] {1, 1, 2, 3, 4};
            ObservableCollection<int> collection = new ObservableCollection<int>();
            Assert.IsEmpty(collection);
            foreach (int i in ints)
                collection.Add(i);
            Assert.AreEqual(ints, collection);
            collection.Remove(1);
            Assert.AreEqual(ints.Skip(1), collection);
            collection.Remove(1);
            Assert.AreEqual(ints.Skip(2), collection);
            collection.Add(1);
            Assert.AreEqual(ints.Skip(2).Concat(new[] {1}), collection);
        }
    }
}
```

### Reference implementation

This implementation makes use of the **null-propagation operator** `?`. The null-propagation operator only evaluates the statement to its right if the expression to its left is **not null**.

```c#
using System.Collections;
using System.Collections.Generic;

namespace Exercises.ObservableCollection
{
    public class ObservableCollection<T> : IEnumerable<T>
    {
        private Node head;
        private Node tail;

        public IEnumerator<T> GetEnumerator()
        {
            for (Node current = head; !(current is null); current = current.Next)
                yield return current.Value;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public event CollectionChanged<T> CollectionChanged;

        public void Add(T item)
        {
            if (head is null)
                head = tail = new Node(item, null);
            else
                tail = tail.Next = new Node(item, null);
            CollectionChanged?.Invoke(this, new CollectionChangedEventArgs<T>(item, CollectionChangedEventType.Add));
        }

        public void Remove(T item)
        {
            for (Node current = head, prev = null; !(current is null); prev = current, current = current.Next)
                if (current.Value.Equals(item))
                {
                    if (!(prev is null))
                        prev.Next = current.Next;
                    else
                        head = head?.Next;

                    CollectionChanged?.Invoke(this,
                        new CollectionChangedEventArgs<T>(item, CollectionChangedEventType.Remove));
                    return;
                }
        }

        private class Node
        {
            public Node(T value, Node next)
            {
                Value = value;
                Next = next;
            }

            public T Value { get; set; }
            public Node Next { get; set; }
        }
    }

    public delegate void CollectionChanged<T>(ObservableCollection<T> collection, CollectionChangedEventArgs<T> args);

    public class CollectionChangedEventArgs<T>
    {
        public CollectionChangedEventArgs(T item, CollectionChangedEventType eventType)
        {
            Item = item;
            EventType = eventType;
        }

        public T Item { get; }
        public CollectionChangedEventType EventType { get; }
    }

    public enum CollectionChangedEventType
    {
        Add,
        Remove
    }
}
```

## 2. Excel cells

Implement an Excel-like `Cell<T>` class, and a `ComputedCell<T>` derived class. 

The `Cell<T>` class holds a value of type `T`, and triggers an event when its value changes. The `ComputedCell<T>` class must modify its value when it recieves a value changed event from another cell.

### Starting code

```c#
using System;

namespace Exercises.ExcelCells
{
    public class Cell<T>
    {
        public T Value { get; set; }
        public event Action<T> ValueChanged;
    }

    public class ComputedCell<T> : Cell<T>
    {
        public ComputedCell(Cell<T> cell, Func<T, T> compute) { }
    }
}
```

### Test code

```c#
using NUnit.Framework;

namespace Exercises.ExcelCells
{
    [TestFixture]
    public class CellTests
    {
        [Test]
        public void TestCellEvent()
        {
            int sum = 0;
            Cell<int> cell = new Cell<int>();
            cell.ValueChanged += i => sum += i;
            cell.Value = 5;
            Assert.AreEqual(5, sum);
            cell.Value = 7;
            Assert.AreEqual(12, sum);
            cell.Value = 7;
            Assert.AreEqual(12, sum);
        }

        [Test]
        public void TestComputedCell()
        {
            Cell<int> cell = new Cell<int>();
            cell.Value = 2;
            ComputedCell<int> computedCell = new ComputedCell<int>(cell, i => i);
            Assert.AreEqual(2, computedCell.Value);
            cell.Value = 5;
            Assert.AreEqual(5, computedCell.Value);
            cell.Value = 17;
            Assert.AreEqual(17, computedCell.Value);
            
            ComputedCell<int> squared = new ComputedCell<int>(cell, i => i * i);
            cell.Value = 2;
            Assert.AreEqual(4, squared.Value);
        }

        [Test]
        public void TestComputedCellCascade()
        {
            Cell<int> cell = new Cell<int>();
            ComputedCell<int> middle = new ComputedCell<int>(cell, i => i + 1);
            ComputedCell<int> end = new ComputedCell<int>(middle, i => i + 1);
            cell.Value = 1;
            Assert.AreEqual(2, middle.Value);
            Assert.AreEqual(3, end.Value);
        }
    }
}
```

### Reference implementation

```c#
using System;

namespace Exercises.ExcelCells
{
    public class Cell<T>
    {
        private T value;

        public T Value
        {
            get => value;
            set
            {
                if (!value.Equals(this.value))
                {
                    this.value = value;
                    ValueChanged?.Invoke(value);
                }
            }
        }

        public event Action<T> ValueChanged;
    }

    public class ComputedCell<T> : Cell<T>
    {
        public ComputedCell(Cell<T> cell, Func<T, T> compute)
        {
            Value = compute(cell.Value);
            cell.ValueChanged += baseCellValue => Value = compute(baseCellValue);
        }
    }
}
```



