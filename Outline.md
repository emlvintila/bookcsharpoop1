[TOC]

# Introduction
## 	The C# language

### Object Oriented Programming

#### Imperative

#### Objects, Data, Procedures

####  Encapsulation, Inheritance, Polymorphism

### Namespaces

### Data types

### The 'Hello World!' program

### Object instances

### Value Types and Reference Types

### Control flow

# Encapsulation

## Fields and Properties

## Methods

## this and static keywords

## Visibility modifiers

1. public
2. internal
3. protected
4. protected internal \*
5. private
6. private protected \*

# Inheritance

## base keyword

## abstract, virtual, override keywords

## sealed keyword

## The diamond problem: inheritance versus composition

### Solution: contain and delegate

# Polymorphism

## Interfaces

## Base Classes and Derived Classes